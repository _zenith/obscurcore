﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using ObscurCore.DTO;
using ObscurCore.Extensions.Generic;
using ObscurCore.Extensions.Streams;

namespace ObscurCore.Packaging
{
    public class PayloadLayoutConfigurationFactory
    {
        /// <summary>
        /// Initialises the configuration for the specified module type with default settings. 
        /// If fine-tuning is desired, use the specialised constructors.
        /// </summary>
        /// <param name="scheme">Desired payload layout scheme.</param>
		public static PayloadLayoutConfiguration CreateDefault(PayloadLayoutSchemes scheme) {
            var config = new PayloadLayoutConfiguration {LayoutSchemeName = scheme.ToString(),
                StreamPRNGName = "Salsa20Generator"
            };

            switch (scheme) {
                case PayloadLayoutSchemes.Simple:
                    break;
                case PayloadLayoutSchemes.Frameshift:
                    // Padding length is variable by default.
                    config.LayoutModuleConfiguration =
                        FrameshiftConfigurationUtility.WriteVariablePadding(
                            FrameshiftPayloadModule.MinimumPaddingLength, FrameshiftPayloadModule.MaximumPaddingLength);
                    break;
                case PayloadLayoutSchemes.Fabric:
                    // Stripe length is variable by default.
                    config.LayoutModuleConfiguration =
                        FabricConfigurationUtility.WriteVariableStriping(FabricPayloadModule.MinimumStripeLength,
                                                                         FabricPayloadModule.MaximumStripeLength);
                    break;
            }

			config.AuxillaryPRNGConfiguration = new byte[] { 0x01 };

            return config;
        }

        /*public static PayloadLayoutConfiguration CreateFrameshift() {
            
        }*/

        /*public static PayloadLayoutConfiguration CreateFabric() {
            
        }*/

    }
	
	
	

    public interface IPayloadModule
    {
        long Assemble ();
        long Disassemble ();
    }



}
