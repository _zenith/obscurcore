namespace ObscurCore.Packaging
{
	public interface IPayloadModule
	{
		long Assemble ();
		long Disassemble ();
	}
}

