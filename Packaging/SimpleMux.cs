using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using ObscurCore.Cryptography.PRNG;
using ObscurCore.DTO;

namespace ObscurCore.Packaging
{
	/// <summary>
	/// Derived stream mux implementing stream selection with PRNG initialised with seed parameters.
	/// </summary>
	public class SimpleMux : StreamMux
	{
		protected PRNGBase prngStream;

		public SimpleMux (bool writing, Stream multiplexedStream, ICollection<IStreamBinding> streams, List<Func<Stream, DecoratingStream>> transforms, 
		                  IPayloadLayoutConfiguration config, int maxOpSize = 16384) : base(writing, multiplexedStream, streams, transforms, maxOpSize)
		{
			prngStream = Athena.Cryptography.PseudoRandomNumberGenerators [config.StreamPRNGName]
			.InstantiateImplementation (config.StreamPRNGConfiguration);

			AdvanceSource ();
		}

		/// <summary>
		/// Advances and returns the index of the next stream to use in an I/O operation (whether to completion or just a buffer-full).
		/// </summary>
		/// <remarks>May be overriden in a derived class to provide for advanced stream selection logic.</remarks>
		/// <returns>The next stream index.</returns>
		protected override sealed int NextSource() {
		    CurrentIndex = prngStream.Next(0, ItemCount - 1);
			return CurrentIndex;
		}
	}
}

