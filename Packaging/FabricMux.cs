using System;
using System.Collections.Generic;
using System.IO;
using ObscurCore.Cryptography.PRNG;
using ObscurCore.DTO;

namespace ObscurCore.Packaging
{
	/// <summary>
	/// Fabric mux.
	/// </summary>
	public class FabricMux : SimpleMux
	{

		public const int 	MinimumStripeLength         = 8,
							MaximumStripeLength         = 65536,
							DefaultFixedStripeLength    = 4096;

		protected readonly PRNGBase prngStripe;
	    protected readonly FabricStripeModes mode;
		protected readonly int minStripe, maxStripe;

		public FabricMux (bool writing, Stream multiplexedStream, List<IStreamBinding> streams, List<Func<Stream, DecoratingStream>> transforms, 
			IPayloadLayoutConfiguration config) : base(writing, multiplexedStream, streams, transforms, config, DefaultFixedStripeLength) // FIX
		{
			FabricConfigurationUtility.Read(config.SchemeConfiguration, out minStripe, out maxStripe);
			if (minStripe < MinimumStripeLength)
				throw new ArgumentOutOfRangeException("config", "Minimum stripe length is set below specification minimum.");
			if (maxStripe > MaximumStripeLength)
				throw new ArgumentOutOfRangeException("config", "Maximum stripe length is set above specification minimum.");

            mode = FabricConfigurationUtility.CheckMode(minStripe, maxStripe);

            if(mode == FabricStripeModes.VariableLength) {
                prngStripe = Athena.Cryptography.PseudoRandomNumberGenerators[config.AuxillaryPRNGName]
                    .InstantiateImplementation(config.AuxillaryPRNGConfiguration);
            }

		    //prngStripe.Next(minStripe, maxStripe);
		}
		
		/// <summary>
		/// If variable striping mode is enabled, advances the state of the stripe length selection PRNG (StripePRNG), 
		/// and returns the length of the next I/O operation to take place.
		/// </summary>
		/// <returns>The operation length.</returns>
		protected override long NextOperationLength() {
		    return mode == FabricStripeModes.VariableLength ? prngStripe.Next(minStripe, maxStripe) : maxStripe;
		}
	}
}

