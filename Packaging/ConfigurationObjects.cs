﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using ObscurCore.Cryptography.PRNG;
using ObscurCore.DTO;
using ObscurCore.Extensions.Generic;
using ObscurCore.Extensions.Streams;

namespace ObscurCore.Packaging
{	
	public class PayloadLayoutConfigurationFactory
	{
		/// <summary>
		/// Initialises the configuration for the specified module type with default settings. 
		/// If fine-tuning is desired, use the specialised constructors.
		/// </summary>
		/// <param name="scheme">Desired payload layout scheme.</param>
		public static PayloadLayoutConfiguration CreateDefault(PayloadLayoutSchemes scheme) {
			var config = new PayloadLayoutConfiguration {
                SchemeName = scheme.ToString(),
				StreamPRNGName = "SOSEMANUK",
				StreamPRNGConfiguration = SOSEMANUKGeneratorConfigurationUtility.WriteRandom()
			};
			
			switch (scheme) {
			case PayloadLayoutSchemes.Simple:
				break;
			case PayloadLayoutSchemes.Frameshift:
				// Padding length is variable by default.
				config.SchemeConfiguration =
					FrameshiftConfigurationUtility.WriteVariablePadding(
						FrameshiftMux.MinimumPaddingLength, FrameshiftMux.MaximumPaddingLength);
				break;
			case PayloadLayoutSchemes.Fabric:
				// Stripe length is variable by default.
				config.SchemeConfiguration =
					FabricConfigurationUtility.WriteVariableStriping(FabricMux.MinimumStripeLength,
					                                                 FabricMux.MaximumStripeLength);
				break;
			}

			if(scheme != PayloadLayoutSchemes.Simple) {
				config.AuxillaryPRNGName = "SOSEMANUK";
				config.AuxillaryPRNGConfiguration = SOSEMANUKGeneratorConfigurationUtility.WriteRandom ();
			}
			
			return config;
		}
		
		/*public static PayloadLayoutConfiguration CreateFrameshift() {
            
        }*/
		
		/*public static PayloadLayoutConfiguration CreateFabric() {
            
        }*/
		
	}

}
