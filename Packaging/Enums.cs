﻿using System;

namespace ObscurCore.Packaging
{
	/// <summary>
	/// Schemes for how the data constituting the payload items is physically arranged (sequence-wise) relative to each other.
	/// </summary>
	public enum PayloadLayoutSchemes
	{
		/// <summary>
		/// Streams written/read in randomly-shuffled order into/from one block, with no further modification.
		/// <para>Lowest security mode, but also the fastest.</para>
		/// </summary>
		Simple,
		/// <summary>
		/// Streams written/read in randomly-shuffled order, with fixed/variable lengths of random data padding the start and end of each.
		/// <para>Medium security mode. Only slightly slower than simple.</para>
		/// </summary>
		Frameshift,
		/// <summary>
		/// Streams written/read in random "striped" pattern, in randomly-shuffled order.
		/// <para>Highest security mode, but also the slowest.</para>
		/// </summary>
		Fabric
	}
	
	public enum FrameshiftPaddingModes
	{
		FixedLength,
		VariableLength
	}

    public enum FabricStripeModes
    {
        FixedLength,
        VariableLength
    }
}
