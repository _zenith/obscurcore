﻿using System;

namespace ObscurCore
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct)]
    public class VersionAttribute : Attribute
    {
        public VersionAttribute(int major, int minor) {
            Major = major; Minor = minor;
        }

        public int Major { get; set; }

        public int Minor { get; set; }

        public override string ToString() {
            return "Version: " + Major + "." + Minor;
        }
    }
}
