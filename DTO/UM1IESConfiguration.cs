using System.IO;
using ProtoBuf;

namespace ObscurCore.DTO
{
    // ***************************************************************************************************************************************************
    // *             This object is not explicitly included in the Manifest supraobject, but may be included in byte-array-serialised form.              *
    // *             They may however incorporate objects in the Manifest superstructure, such as a SymmetricCipherConfiguration or similar.             *
    // ***************************************************************************************************************************************************

    [ProtoContract]
    public class UM1IESConfiguration : IManifestCryptographySchemeConfiguration
    {
        /// <summary>
        /// Ephemeral key to be used in UM1 key exchange calculations to produce a shared secret.
        /// </summary>
        [ProtoMember(1)]
        public ECKeyConfiguration EphemeralKey { get; set; }
		
        /// <summary>
        /// Configuration for the symmetric cipher to use with the key derived from the shared secret.
        /// </summary>
        [ProtoMember(2)]
        public SymmetricCipherConfiguration SymmetricCipher { get; set; }
		
        /// <summary>
        /// Configuration for the scheme used to derive a key from the shared secret.
        /// </summary>
        [ProtoMember(3)]
        public KeyDerivationConfiguration KeyDerivation { get; set; }

        public byte[] AdditionalData { get; set; }

        public override bool Equals (object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((UM1IESConfiguration) obj);
        }

        public bool Equals(UM1IESConfiguration other) {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            if(!IsSuperficiallyValid()) 
                throw new InvalidDataException("Not a valid key agreement configuration.");
            return EphemeralKey.Equals(other.EphemeralKey) && SymmetricCipher.Equals(other.SymmetricCipher) 
                   && KeyDerivation.Equals(other.KeyDerivation);
        }

        public override int GetHashCode () {
            if (!IsSuperficiallyValid())
                throw new InvalidDataException("Not a valid key agreement configuration.");
            unchecked {
                int hashCode = EphemeralKey.GetHashCode(); // Must not be null! 
                hashCode = (hashCode * 397) ^ SymmetricCipher.GetHashCode(); // Must not be null! 
                hashCode = (hashCode * 397) ^ KeyDerivation.GetHashCode(); // Must not be null! 
                return hashCode;
            }
        }
        // TODO: Use this concept on the other DTO objects, it's useful to have
        public bool IsSuperficiallyValid() {
            return EphemeralKey != null && SymmetricCipher != null && KeyDerivation != null;
        }
    }
}