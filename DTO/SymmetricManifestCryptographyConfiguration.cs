using System.IO;
using ProtoBuf;

namespace ObscurCore.DTO
{
    // ***************************************************************************************************************************************************
    // *             This object is not explicitly included in the Manifest supraobject, but may be included in byte-array-serialised form.              *
    // *             They may however incorporate objects in the Manifest superstructure, such as a SymmetricCipherConfiguration or similar.             *
    // ***************************************************************************************************************************************************

    /// <summary>
    /// Configuration of a symmetric cryptosystem to secure a package manifest.
    /// </summary>
    [ProtoContract]
    public class SymmetricManifestCryptographyConfiguration : IManifestCryptographySchemeConfiguration
    {
        /// <summary>
        /// Encryption configuration for the manifest.
        /// </summary>
        [ProtoMember(1, IsRequired = true)]
        public SymmetricCipherConfiguration SymmetricCipher { get; set; }

        /// <summary>
        /// Key confirmation configuration for the manifest.
        /// </summary>
        [ProtoMember(2, IsRequired = true)]
        public KeyConfirmationConfiguration KeyVerification { get; set; }

        /// <summary>
        /// Key derivation configuration for the manifest.
        /// </summary>
        [ProtoMember(3, IsRequired = true)]
        public KeyDerivationConfiguration KeyDerivation { get; set; }

        public override bool Equals (object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((SymmetricManifestCryptographyConfiguration) obj);
        }

        public bool Equals (SymmetricManifestCryptographyConfiguration other) {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return SymmetricCipher.Equals(other.SymmetricCipher) && KeyVerification.Equals(other.KeyVerification) && KeyDerivation.Equals(other.KeyDerivation);
        }

        public override int GetHashCode () {
            if (!IsSuperficiallyValid())
                throw new InvalidDataException("Not a valid manifest cryptography configuration.");
            unchecked {
                int hashCode = SymmetricCipher.GetHashCode();
                hashCode = (hashCode * 397) ^ KeyVerification.GetHashCode();
                hashCode = (hashCode * 397) ^ KeyDerivation.GetHashCode();
                return hashCode;
            }
        }

        public bool IsSuperficiallyValid() { return (SymmetricCipher == null || KeyVerification == null || KeyDerivation == null); }
    }
}