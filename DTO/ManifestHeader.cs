using System;
using System.Linq;
using ProtoBuf;

namespace ObscurCore.DTO
{
    [ProtoContract]
    public class ManifestHeader : IEquatable<ManifestHeader>
    {
        [ProtoMember(1)]
        public int FormatVersion { get; set; }

        /// <summary>
        /// Compression configuration for the manifest.
        /// </summary>
        [ProtoMember(2, IsRequired = false)]
        public CompressionConfiguration Compression { get; set; }

        [ProtoMember(3, IsRequired = true)]
        public string CryptographySchemeName { get; set; }
		
        /// <summary>
        /// Configuration for the cryptography-scheme-specific payload I/O module.
        /// </summary>
        /// <remarks>Format of the configuration is that of the consuming type.</remarks>
        [ProtoMember(4, IsRequired = false)]
        public byte[] CryptographySchemeConfiguration { get; set; }

        /// <summary>
        /// Whether to cache the key used for the manifest for the duration that the package is being operated on. 
        /// Only use this if the same key is to be used in payload items - as is, or further derived.
        /// </summary>
        /// <remarks>This prevents unnecessary computational resource being used, as key derivation and/or agreement is expensive.</remarks>
        [ProtoMember(5)]
        public bool CacheKeyForContext { get; set; }

        public override bool Equals (object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ManifestHeader) obj);
        }
		
        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other"/> parameter; otherwise, false.
        /// </returns>
        /// <param name="other">An object to compare with this object.</param>
        public bool Equals (ManifestHeader other) {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return FormatVersion == other.FormatVersion && Compression.Equals(other.Compression) && string.Equals(CryptographySchemeName, other.CryptographySchemeName) &&
                   (CryptographySchemeConfiguration == null ? other.CryptographySchemeConfiguration == null : CryptographySchemeConfiguration.SequenceEqual(other.CryptographySchemeConfiguration));
        }
		
        /// <summary>
        /// Serves as a hash function for a particular type. 
        /// </summary>
        /// <returns>
        /// A hash code for the current <see cref="T:System.Object"/>.
        /// </returns>
        /// <filterpriority>2</filterpriority>
        public override int GetHashCode () {
            unchecked {
                return (CryptographySchemeName.GetHashCode() * 397) ^ (CryptographySchemeConfiguration != null ? CryptographySchemeConfiguration.GetHashCode() : 0);
            }
        }
    }
}