using ProtoBuf;

namespace ObscurCore.DTO
{
	#region Cryptography
	[ProtoContract]
	public enum SymmetricCipherType
	{
		None,
		AEAD,
		Block,
		Stream
	}

	public enum KeyActions
	{
		Associate,
		Dissociate,
		/// <summary>
		/// Reserved for use. For a scheme where key state can be 
		/// verified with state at another session-state locality.
		/// </summary>
		Validate,
		/// <summary>
		/// Reserved for use. For a scheme where keys change state 
		/// deterministically at multiple session-state localities.
		/// </summary>
		Advance
	}

    public enum ManifestCryptographySchemes
    {
		/// <summary>
		/// UM1-based Integrated Encryption Scheme. U
		/// </summary>
		/// <remarks>
		/// Uses UM1 to generate a secret value, which is further derived with a KDF. 
		/// This derived secret is used as a symmetric cipher key, and to generate a MAC for the data.
		/// </remarks>
		UM1IES,

        /// <summary>
        /// Using a pre-agreed key. Simply uses a SymmetricCipherConfiguration object in serialised form for configuration, and as such, allows rich customisation.
        /// </summary>
        UniversalSymmetric
    }
	#endregion

	#region Packaging
	/// <summary>
	/// Possible distinct types of payload item that should/can be treated differently by an application.
	/// </summary>
	public enum PayloadItemTypes
	{
		Binary = 0,
		UTF8,
        UTF32,
		KeyAction
	}
	#endregion
}
