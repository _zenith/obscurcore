#!/bin/bash
printf "\nReady to create the serialiser assembly?\nType 'yes' to create, anything else will cancel. -> "
read ANSWER
if [ "$ANSWER" = "yes" ] ; then
	echo "Running precompile under mono..."
	mono ./Release/Precompile/precompile.exe ./Release/ObscurCore.DTO.dll -o:./Release/ObscurCore.DTOSerialiser.dll -t:ObscurCore.DTOSerialiser
else
	echo "Canceled!"
fi