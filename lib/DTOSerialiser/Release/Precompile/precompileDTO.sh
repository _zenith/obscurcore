#!/bin/sh
printf "\nReady to create the serialiser assembly?\nType 'yes' to create, anything else will cancel. -> "
read ANSWER
if [ "$ANSWER" = "yes" ] ; then
	echo "Running precompile under mono..."
	mono precompile.exe ../ObscurCore.DTO.dll -o:../ObscurCore.DTOSerialiser.dll -t:ObscurCore.DTOSerialiser
else
	echo "Canceled!"
fi