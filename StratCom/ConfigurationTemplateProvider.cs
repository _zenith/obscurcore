﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ObscurCore.Cryptography;
using ObscurCore.Compression;

namespace ObscurCore.StratCom
{
    /// <summary>
    /// Uses existing configuration objects (called "manifests" when serialised in JSON) as a template, merging another manifest's non-null data with it.
    /// </summary>
    class ConfigurationTemplateProvider
    {
    }

    /// <summary>
    /// Also called a profile in ObscurCore-nomenclature.
    /// </summary>
    public static class ConfigurationTemplates
    {
        #region Methods for getting package profile aspects
        public static SymmetricCipherConfiguration GetPayloadCipherConfiguration(PackageProfiles profile) {
            switch (profile)
            {
                case PackageProfiles.Standard:
                    return GetSymmetricCipherConfiguration(SymmetricCipherProfiles.Standard);
                case PackageProfiles.MinimalResourceUse:
                    return GetSymmetricCipherConfiguration(SymmetricCipherProfiles.MinimalResourceUse);
                default:
                    throw new NotImplementedException("Profile is not implemented.");
            }
        }

        public static CompressionConfiguration GetCompressionConfiguration (PackageProfiles profile) {
            switch (profile) {
                case PackageProfiles.Standard:
                    return GetCompressionConfiguration(CompressionProfiles.Deflate);
                case PackageProfiles.MinimalResourceUse:
                    return GetCompressionConfiguration(CompressionProfiles.LZ4);
                default:
                    throw new NotImplementedException("Profile is not implemented.");
            }
        }
        #endregion

        #region Methods for getting cryptography profiles
        public static SymmetricCipherConfiguration GetSymmetricCipherConfiguration(SymmetricCipherProfiles profile) {
            switch (profile) {
                case SymmetricCipherProfiles.Authenticated:
                return new SymmetricCipherConfiguration(SymmetricBlockCiphers.AES, AEADBlockCipherModes.GCM, null, 256, 128);
                case SymmetricCipherProfiles.MinimalResourceUse:
                    return new SymmetricCipherConfiguration(SymmetricStreamCiphers.Salsa20, 256);
                case SymmetricCipherProfiles.Standard:
                    return new SymmetricCipherConfiguration(SymmetricBlockCiphers.AES, BlockCipherModes.CTR, null, 128, 256);
                default:
                    throw new NotImplementedException("Profile is not implemented.");
            }
        }


        #endregion

        public static CompressionConfiguration GetCompressionConfiguration (CompressionProfiles profile) {
            switch (profile) {
            case CompressionProfiles.Bzip2:
                return new CompressionConfiguration(CompressionAlgorithms.Bzip2) {
                    Bzip2BlockSize = 9
                };
            case CompressionProfiles.Deflate:
                return new CompressionConfiguration(CompressionAlgorithms.Deflate) {
                    DeflateLevel = 8
                };
            case CompressionProfiles.LZ4:
                return new CompressionConfiguration(CompressionAlgorithms.LZ4) {
                    LZ4BlockSize = 32768
                };
            default:
                throw new NotImplementedException("Profile is not implemented.");
            }
        }

    }






    public enum PackageProfiles
    {
        /// <summary>
        /// Bzip2 (900kB block size) compression, AES-CTR (256-bit key) encryption, Scrypt key derivation, fabric payload.
        /// </summary>
        Standard,
        /// <summary>
        /// LZ4 (32kB block size) compression, AES-CTR (256-bit key) encryption, PBKDF2 key derivation, fabric payload.
        /// </summary>
        MobileStandard,
        /// <summary>
        /// LZ4 (32kB block size) compression, 256-bit Salsa20 encryption, PBKDF2 key derivation, frameshift payload.
        /// </summary>
        MinimalResourceUse
    }

    public enum SymmetricCipherProfiles
    {
        /// <summary>256-bit AES-CTR, Scrypt key derivation.</summary>
        Standard,
        /// <summary>256-bit AES-CTR, PBKDF2 key derivation.</summary>
        MobileStandard,
        /// <summary>256-bit Salsa20, PBKDF2 key derivation.</summary>
        MinimalResourceUse,
        /// <summary>256-bit AES-GCM with 128-bit MAC, Scrypt key derivation.</summary>
        Authenticated
    }

    public enum CompressionProfiles
    {
        /// <summary>Bzip2 compression with a 900kB block size.</summary>
        Bzip2,
        /// <summary>Deflate compression in level 8 mode.</summary>
        Deflate,
        /// <summary>LZ4 compression with a 32kB byte block size.</summary>
        LZ4
    }


}
