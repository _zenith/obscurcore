﻿using System;
using System.Collections.Generic;

using ObscurCore.Cryptography;
using ObscurCore.Cryptography.BouncyCastle.Crypto;
using ObscurCore.Cryptography.BouncyCastle.Crypto.Engines;
using ObscurCore.Cryptography.BouncyCastle.Crypto.Modes;
using ObscurCore.Cryptography.BouncyCastle.Crypto.Paddings;
using ObscurCore.Cryptography.BouncyCastle.Crypto.Parameters;
using ObscurCore.Cryptography.BouncyCastle.Math;
using ObscurCore.Cryptography.BouncyCastle.Math.EC;
using ObscurCore.Cryptography.Ciphers;
using ObscurCore.Cryptography.Digests;
using ObscurCore.Cryptography.Digests.SHA3;
using ObscurCore.Cryptography.MACs;
using ObscurCore.Cryptography.PRNG;
using ObscurCore.Cryptography.KDF;
using ObscurCore.DTO;
using ObscurCore.Extensions.Enumerations;

namespace ObscurCore.Knowledge
{
    /// <summary>
	/// Stores a description of and means of instantiating a symmetric cipher - 
	/// its name, what parameters it requires, etc.
    /// </summary>
    public sealed class SymmetricCipherDescription
    {
        private readonly static Dictionary<SymmetricBlockCiphers, Func<int, IBlockCipher>> _engineInstantiatorsBlock =
            new Dictionary<SymmetricBlockCiphers, Func<int, IBlockCipher>>();
        private readonly static Dictionary<SymmetricStreamCiphers, Func<IStreamCipher>> _engineInstantiatorsStream =
            new Dictionary<SymmetricStreamCiphers, Func<IStreamCipher>>();

        static SymmetricCipherDescription () {
            // Add all engine Func delegates via lambdas
            // Block engines
#if (MOBILE && RESOURCE_CONSTRAINED)
            _engineInstantiatorsBlock.Add(SymmetricBlockCiphers.AES, blockSize => new AesEngine()); // might be unnecessary - made dependent on two ifdefs for this reason!
#else
            _engineInstantiatorsBlock.Add(SymmetricBlockCiphers.AES, blockSize => new AesFastEngine()); // fast implementation with static tables :-)
#endif
            _engineInstantiatorsBlock.Add(SymmetricBlockCiphers.Blowfish, blockSize => new BlowfishEngine());
            _engineInstantiatorsBlock.Add(SymmetricBlockCiphers.CAST5, blockSize => new Cast5Engine());
            _engineInstantiatorsBlock.Add(SymmetricBlockCiphers.CAST6, blockSize => new Cast6Engine());
            _engineInstantiatorsBlock.Add(SymmetricBlockCiphers.Camellia, blockSize => new CamelliaEngine());
            _engineInstantiatorsBlock.Add(SymmetricBlockCiphers.GOST28147, blockSize => new Gost28147Engine());
            _engineInstantiatorsBlock.Add(SymmetricBlockCiphers.IDEA, blockSize => new IdeaEngine());
            _engineInstantiatorsBlock.Add(SymmetricBlockCiphers.NOEKEON, blockSize => new NoekeonEngine());
            _engineInstantiatorsBlock.Add(SymmetricBlockCiphers.RC6, blockSize => new RC6Engine());
            _engineInstantiatorsBlock.Add(SymmetricBlockCiphers.Rijndael, blockSize => new RijndaelEngine(blockSize));
            _engineInstantiatorsBlock.Add(SymmetricBlockCiphers.Serpent, blockSize => new SerpentEngine());
            _engineInstantiatorsBlock.Add(SymmetricBlockCiphers.TripleDES, blockSize => new DesEdeEngine());
            _engineInstantiatorsBlock.Add(SymmetricBlockCiphers.Twofish, blockSize => new TwofishEngine());
            // Stream engines
            _engineInstantiatorsStream.Add(SymmetricStreamCiphers.HC128, () => new HC128Engine());
            _engineInstantiatorsStream.Add(SymmetricStreamCiphers.HC256, () => new HC256Engine());
            _engineInstantiatorsStream.Add(SymmetricStreamCiphers.Rabbit, () => new RabbitEngine());
            _engineInstantiatorsStream.Add(SymmetricStreamCiphers.RC4, () => new RC4Engine());
            _engineInstantiatorsStream.Add(SymmetricStreamCiphers.Salsa20, () => new Salsa20Engine());
            _engineInstantiatorsStream.Add(SymmetricStreamCiphers.SOSEMANUK, () => new SOSEMANUKEngine());
            _engineInstantiatorsStream.Add(SymmetricStreamCiphers.VMPC, () => new VmpcEngine());
        }

        /// <summary>
        /// Instantiates and returns a symmetric block cipher of the algorithm type that the instance this method was called from describes.
        /// </summary>
        /// <returns>An IBlockCipher cipher object implementing the relevant cipher algorithm.</returns>
        public IBlockCipher InstantiateBlockImplementation (int? blockSize = null) {
            SymmetricBlockCiphers cipher;
            if (!Enum.TryParse<SymmetricBlockCiphers>(Name, true, out cipher))
                throw new NotSupportedException("Cipher is unknown or otherwise unsupported.");
            return _engineInstantiatorsBlock[cipher](blockSize ?? DefaultBlockSize);
        }

        /// <summary>
        /// Instantiates and returns a symmetric stream cipher of the algorithm type that the instance this method was called from describes.
        /// </summary>
        /// <returns>An IStreamCipher cipher object implementing the relevant cipher algorithm.</returns>
        public IStreamCipher InstantiateStreamImplementation () {
            SymmetricStreamCiphers cipher;
            if (!Enum.TryParse<SymmetricStreamCiphers>(Name, true, out cipher))
                throw new NotSupportedException("Cipher is unknown or otherwise unsupported.");
            return _engineInstantiatorsStream[cipher]();
        }

        /// <summary>
        /// Name of the cryptographic cipher transform (must be a member of SymmetricBlockCiphers or SymmetricStreamCiphers).
        /// </summary>
        public string Name { get; internal set; }

        /// <summary>
        /// Name to show a user or for a detailed specification.
        /// </summary>
        public string DisplayName { get; internal set; }

        /// <summary>
        /// Array of allowable sizes (in bits) for the block size of the cipher, where applicable. Set to -1 if stream cipher.
        /// </summary>
        public int[] AllowableBlockSizes { get; internal set; }

        /// <summary>
        /// If no block size size is supplied when configuring the cipher, this is the size that should be used, where applicable. Set to -1 if stream cipher.
        /// </summary>
        public int DefaultBlockSize { get; internal set; }

        /// <summary>
        /// Array of allowable sizes (in bits) for the cipher initialisation vector (IV).
        /// </summary>
        public int[] AllowableIVSizes { get; internal set; }

        /// <summary>
        /// If no IV size is supplied when configuring the cipher, this is the size that should be used.
        /// </summary>
        public int DefaultIVSize { get; internal set; }

        /// <summary>
        /// Array of allowable sizes (in bits) for the cryptographic key.
        /// </summary>
        public int[] AllowableKeySizes { get; internal set; }

        /// <summary>
        /// If no key size is supplied when configuring the cipher, this is the size that should be used.
        /// </summary>
        public int DefaultKeySize { get; internal set; }
    }

    /// <summary>
	/// Stores a description of and means of instantiating a mode of operation for 
	/// a symmetric block cipher - its name, what parameters it requires, etc.
    /// </summary>
    public sealed class SymmetricCipherModeDescription
    {
        private readonly static Dictionary<BlockCipherModes, Func<IBlockCipher, int, IBlockCipher>> _modeInstantiatorsBlock =
            new Dictionary<BlockCipherModes, Func<IBlockCipher, int, IBlockCipher>>();
        private readonly static Dictionary<AEADBlockCipherModes, Func<IBlockCipher, IAeadBlockCipher>> _modeInstantiatorsAEAD =
            new Dictionary<AEADBlockCipherModes, Func<IBlockCipher, IAeadBlockCipher>>();

        static SymmetricCipherModeDescription () {
            // Add all mode Func delegates via lambdas
            _modeInstantiatorsBlock.Add(BlockCipherModes.CBC, (cipher, size) => new CbcBlockCipher(cipher));
            _modeInstantiatorsBlock.Add(BlockCipherModes.CFB, (cipher, size) => new CfbBlockCipher(cipher, size));
            _modeInstantiatorsBlock.Add(BlockCipherModes.CTR, (cipher, size) => new SicBlockCipher(cipher));
            // CTS is not properly supported here...
            // Interim solution is just to return a CBC mode cipher, then it can be transformed into a CTS cipher afterwards. The return type is non-compatible :( .
            _modeInstantiatorsBlock.Add(BlockCipherModes.CTS_CBC, (cipher, size) => new CbcBlockCipher(cipher));
            _modeInstantiatorsBlock.Add(BlockCipherModes.OFB, (cipher, size) => new OfbBlockCipher(cipher, size));
            // AEAD modes
            _modeInstantiatorsAEAD.Add(AEADBlockCipherModes.EAX, cipher => new EaxBlockCipher(cipher));
            _modeInstantiatorsAEAD.Add(AEADBlockCipherModes.GCM, cipher => new GcmBlockCipher(cipher));
            //_modeInstantiatorsAEAD.Add(AEADBlockCipherModes.SIV, cipher => new SivBlockCipher(cipher));
			//_modeInstantiatorsAEAD.Add(AEADBlockCipherModes.OCB, cipher => new OcbBlockCipher(cipher));
        }

        /// <summary>
        /// Instantiates and returns a block cipher implementing the mode of operation that the instance this method was called from describes.
        /// </summary>
        /// <param name="cipher">The block cipher to implement this mode of operation on top of.</param>
        /// <param name="cipherName">The cipher name this is to be used with, for .</param>
        /// <param name="size">Where applicable, the size parameter required for some modes of operation.</param>
        /// <returns>
        /// An IBlockCipher cipher object implementing the relevant mode of operation on top of the supplied symmetric block cipher.
        /// </returns>
        public IBlockCipher InstantiateImplementation (IBlockCipher cipher, string cipherName, int? size = null) {
            BlockCipherModes mode;
            Name.ToEnum(out mode);
            //return _modeInstantiatorsBlock[mode](cipher, size / 8 ?? 
                //(cipher != null ? Athena.Cryptography.SymmetricCiphers[cipherName].DefaultBlockSize / 8 : cipher.GetBlockSize() / 8));

            var cipherMode = _modeInstantiatorsBlock[mode](cipher, size ??
                (cipher != null ? Athena.Cryptography.SymmetricCiphers[cipherName].DefaultBlockSize : cipher.GetBlockSize()));
            //return mode != BlockCipherModes.CTS_CBC ? cipherMode : new CtsBlockCipher(cipher) as IBlockCipher;
            return cipherMode;
        }

        /// <summary>
        /// Instantiates and returns an AEAD block cipher implementing the mode of operation that the instance this method was called from describes.
        /// </summary>
        /// <param name="cipher">The block cipher to implement this mode of operation on top of.</param>
        /// <returns>
        /// An IAeadBlockCipher cipher object implementing the relevant mode of operation on top of the supplied symmetric block cipher.
        /// </returns>
        public IAeadBlockCipher InstantiateImplementation (IBlockCipher cipher) {
            if (!IsAEADMode) throw new NotSupportedException("Cannot return an AEAD object for a non-AEAD mode of operation.");
            AEADBlockCipherModes mode;
            Name.ToEnum(out mode);
            return _modeInstantiatorsAEAD[mode](cipher);
        }

        /// <summary>
        /// Name of the cryptographic cipher mode (must be a member of BlockCipherModes or AEADBlockCipherModes).
        /// </summary>
        public string Name { get; internal set; }

        /// <summary>
        /// Name to show a user or for a detailed specification.
        /// </summary>
        public string DisplayName { get; internal set; }

        /// <summary>
        /// Array of allowable sizes (in bits) for the block size of the cipher. Set to -1 if unrestricted.
        /// </summary>
        public int[] AllowableBlockSizes { get; internal set; }

        /// <summary>
        /// Whether this mode requires padding.
        /// </summary>
        public PaddingRequirements PaddingRequirement { get; internal set; }

        /// <summary>
        /// Whether this mode is of the Authenticated Encryption/Decryption type.
        /// </summary>
        public bool IsAEADMode { get; internal set; }

        /// <summary>
        /// Whether the nonce/IV can be re-used in a later encryption operation, where data 
        /// will travel over the same channel, or otherwise might be subjected to analysis.
        /// </summary>
        public NonceReusePolicies NonceReusePolicy { get; internal set; }
    }

    public enum PaddingRequirements
    {
        /// <summary>
        /// Self-explanatory.
        /// </summary>
        None = 0,
        /// <summary>
        /// Padding scheme must be used if plaintext length is less than 1 block length.
        /// </summary>
        IfUnderOneBlock,
        /// <summary>
        /// Self-explanatory.
        /// </summary>
        Always
    }

    public enum NonceReusePolicies
    {
        /// <summary>
        /// Self-explanatory.
        /// </summary>
        NotApplicable = 0,
        /// <summary>
        /// Nonce reuse may result in total or partial loss of security properties.
        /// </summary>
        NotAllowed,
        /// <summary>
        /// Construction of operation mode allows nonce reuse without catastrophic security loss, 
        /// but better security properties will be obtained by ensuring a new nonce is used each time.
        /// </summary>
        Allowed
    }

    /// <summary>
    /// Stores a description of and means of instantiating a padding scheme for a symmetric cipher.
    /// </summary>
    public sealed class SymmetricCipherPaddingDescription
    {
        private readonly static Dictionary<BlockCipherPaddingTypes, Func<IBlockCipherPadding>> _paddingInstantiators =
            new Dictionary<BlockCipherPaddingTypes, Func<IBlockCipherPadding>>();

        static SymmetricCipherPaddingDescription () {
            _paddingInstantiators.Add(BlockCipherPaddingTypes.ISO10126D2, () => new ISO10126d2Padding());
            _paddingInstantiators.Add(BlockCipherPaddingTypes.ISO7816D4, () => new ISO7816d4Padding());
            _paddingInstantiators.Add(BlockCipherPaddingTypes.PKCS7, () => new ISO10126d2Padding());
            _paddingInstantiators.Add(BlockCipherPaddingTypes.TBC, () => new ISO10126d2Padding());
            _paddingInstantiators.Add(BlockCipherPaddingTypes.X923, () => new ISO10126d2Padding());
        }

        /// <summary>
        /// Instantiates and returns a IBlockCipherPadding object implementing the relevant padding mode for interaction with a block cipher.
        /// </summary>
        /// <returns>
        /// An IBlockCipherPadding cipher object implementing the relevant padding scheme for use with symmetric block ciphers.
        /// </returns>
        public IBlockCipherPadding InstantiateImplementation () {
            BlockCipherPaddingTypes padding;
            Name.ToEnum(out padding);
            return _paddingInstantiators[padding]();
        }

        /// <summary>
        /// Name of the cryptographic cipher padding scheme (must be a member of BlockCipherPaddingTypes).
        /// </summary>
        public string Name { get; internal set; }

        /// <summary>
        /// Name to show a user or for a detailed specification.
        /// </summary>
        public string DisplayName { get; internal set; }
    }
	
	/// <summary>
	/// Stores a description of and means of deriving a key from a key derivation function.
	/// </summary>
	public sealed class KDFDescription
	{
		private readonly static Dictionary<KeyDerivationFunctions, Func<int, byte[], IKDFModule>> _kdfInstantiators =
			new Dictionary<KeyDerivationFunctions, Func<int, byte[], IKDFModule>>();
		
		private readonly static Dictionary<KeyDerivationFunctions, Func<byte[], byte[], int, byte[], byte[]>> _kdfStatics =
			new Dictionary<KeyDerivationFunctions, Func<byte[], byte[], int, byte[], byte[]>>();
		
		static KDFDescription () {
			_kdfInstantiators.Add(KeyDerivationFunctions.PBKDF2, (outputSize, config) => new PBKDF2Module(outputSize, config));
			_kdfInstantiators.Add(KeyDerivationFunctions.Scrypt, (outputSize, config) => new ScryptModule(outputSize, config));
			
			_kdfStatics.Add(KeyDerivationFunctions.PBKDF2, PBKDF2Module.DeriveKeyWithConfig);
			_kdfStatics.Add(KeyDerivationFunctions.Scrypt, ScryptModule.DeriveKeyWithConfig);
		}
		
		/// <summary>
		/// Derives a working key with the KDF module.
		/// </summary>
		/// <returns>The working key.</returns>
		/// <param name="key">Pre-key to use as input material.</param>
		/// <param name="salt">Salt to use in derivation to increase entropy.</param>
		/// <param name="outputSize">Output key size in bits.</param>
		/// <param name="config">Configuration of the KDF in byte-array encoded form.</param>
		public byte[] DeriveKey (byte[] key, byte[] salt, int outputSize, byte[] config) {
			KeyDerivationFunctions kdf;
			Name.ToEnum(out kdf);
			return _kdfStatics[kdf](key, salt, outputSize / 8, config);
		}
		
		public IKDFModule InstantiateImplementation(int outputSize, byte[] config) {
			KeyDerivationFunctions kdf;
			Name.ToEnum(out kdf);
			return _kdfInstantiators[kdf](outputSize, config);
		}
		
		/// <summary>
		/// Name of the KDF scheme (must be a member of KeyDerivationFunctions).
		/// </summary>
        public string Name { get; internal set; }

		/// <summary>
		/// Name to show a user or for a detailed specification.
		/// </summary>
        public string DisplayName { get; internal set; }
	}
	
	/// <summary>
	/// Stores a description of and means of instantiating a pseudorandom number generator.
	/// </summary>
	public sealed class PRNGDescription
	{
		private readonly static Dictionary<PseudoRandomNumberGenerators, Func<byte[], PRNGBase>> _prngInstantiators =
			new Dictionary<PseudoRandomNumberGenerators, Func<byte[], PRNGBase>>();
		
		static PRNGDescription () {
			_prngInstantiators.Add(PseudoRandomNumberGenerators.Salsa20, Salsa20Generator.CreateFromConfiguration);
			_prngInstantiators.Add(PseudoRandomNumberGenerators.SOSEMANUK, SOSEMANUKGenerator.CreateFromConfiguration);
		}
		
		/// <summary>
		/// Instantiates and returns a PRNG implementing the mode of generation that the
		/// instance this method was called from describes.
		/// </summary>
		/// <param name="config">Configuration of the PRNG in byte-array encoded form.</param>
		/// <returns>
		/// An PRNG object deriving from Random.
		/// </returns>
		public PRNGBase InstantiateImplementation (byte[] config) {
			PseudoRandomNumberGenerators prng;
			Name.ToEnum(out prng);
			return _prngInstantiators[prng](config);
		}
		
		/// <summary>
		/// Name of the PRNG scheme (must be a member of PseudoRandomNumberGenerators).
		/// </summary>
		public string Name { get; internal set; }
		/// <summary>
		/// Name to show a user or for a detailed specification.
		/// </summary>
		public string DisplayName { get; internal set; }
	}

	/*public sealed class ManifestCryptographySchemeDescription
	{
		private readonly static Dictionary<ManifestCryptographySchemes, Func<byte[], Random>> _mschemeInstantiators =
			new Dictionary<ManifestCryptographySchemes, Func<byte[], Random>>();
		
		static ManifestCryptographySchemeDescription () {
            _mschemeInstantiators.Add(ManifestCryptographySchemes.UM1IES, Salsa20Generator.CreateFromConfiguration);
		}
		
		/// <summary>
		/// Instantiates and returns a PRNG implementing the mode of generation that the
		/// instance this method was called from describes.
		/// </summary>
		/// <param name="config">Configuration of the PRNG in byte-array encoded form.</param>
		/// <returns>
		/// An PRNG object deriving from Random.
		/// </returns>
		public Random InstantiateImplementation (byte[] config) {
            ManifestCryptographySchemes prng;
			Name.ToEnum(out prng);
			return _mschemeInstantiators[prng](config);
		}

		/// <summary>
		/// Name of the PRNG scheme (must be a member of PseudoRandomNumberGenerators).
		/// </summary>
		public string Name { get; internal set; }
		/// <summary>
		/// Name to show a user or for a detailed specification.
		/// </summary>
		public string DisplayName { get; internal set; }
	}*/

	/// <summary>
	/// Stores a description of and means of instantiating a hash/digest function primitive.
	/// </summary>
	public sealed class HashFunctionDescription
	{
		private readonly static Dictionary<HashFunctions, Func<IDigest>> _digestInstantiators =
			new Dictionary<HashFunctions, Func<IDigest>>();
		
		static HashFunctionDescription () {
			_digestInstantiators.Add(HashFunctions.BLAKE2B256, () => new BLAKE2BDigest(256, true, false));
			_digestInstantiators.Add(HashFunctions.BLAKE2B384, () => new BLAKE2BDigest(384, true, false));
			_digestInstantiators.Add(HashFunctions.BLAKE2B512, () => new BLAKE2BDigest(512, true, false));

			_digestInstantiators.Add(HashFunctions.Keccak224, () => new KeccakManaged(224, true));
			_digestInstantiators.Add(HashFunctions.Keccak256, () => new KeccakManaged(256, true));
			_digestInstantiators.Add(HashFunctions.Keccak384, () => new KeccakManaged(384, true));
			_digestInstantiators.Add(HashFunctions.Keccak512, () => new KeccakManaged(512, true));
		}
		
		/// <summary>
		/// Instantiates and returns a hash/digest primitive.
		/// </summary>
		/// <param name="config">Configuration of the PRNG in byte-array encoded form.</param>
		/// <returns>
		/// An PRNG object deriving from Random.
		/// </returns>
		public IDigest InstantiateImplementation () {
			HashFunctions hash;
			Name.ToEnum(out hash);
			return _digestInstantiators[hash]();
		}
		
		/// <summary>
		/// Name of the PRNG scheme (must be a member of PseudoRandomNumberGenerators).
		/// </summary>
		public string Name { get; internal set; }
		/// <summary>
		/// Name to show a user or for a detailed specification.
		/// </summary>
		public string DisplayName { get; internal set; }
	}

	/// <summary>
	/// Stores a description of and means of instantiating a Message Authentication Code (MAC) generator primitive.
	/// </summary>
	public sealed class MACFunctionDescription
	{
		private readonly static Dictionary<MACFunctions, Func<IMac>> _macInstantiators =
			new Dictionary<MACFunctions, Func<IMac>>();
		
		static MACFunctionDescription () {
			_macInstantiators.Add(MACFunctions.BLAKE2B256, () => new Blake2BMac(256, true, false));
			_macInstantiators.Add(MACFunctions.BLAKE2B384, () => new Blake2BMac(384, true, false));
			_macInstantiators.Add(MACFunctions.BLAKE2B512, () => new Blake2BMac(512, true, false));

			_macInstantiators.Add(MACFunctions.Keccak224, () => new KeccakMac(224, true));
			_macInstantiators.Add(MACFunctions.Keccak256, () => new KeccakMac(256, true));
			_macInstantiators.Add(MACFunctions.Keccak384, () => new KeccakMac(384, true));
			_macInstantiators.Add(MACFunctions.Keccak512, () => new KeccakMac(512, true));
		}
		
		/// <summary>
		/// Instantiates and returns a Message Authentication Code (MAC) generator primitive.
		/// </summary>
		/// <param name="config">Cryptographic key to use in the MAC operation.</param>
		/// <param name="config">Cryptographic salt to use in the MAC operation, where supported.</param>
		/// <returns>
		/// An MAC object deriving from IMac.
		/// </returns>
		public IMac InstantiateImplementation (byte[] key, byte[] salt = null) {
			MACFunctions mac;
			Name.ToEnum(out mac);
			var macObj = _macInstantiators[mac]();

			if(SaltSupported) {
				((IMacWithSalt)macObj).Init(key, salt);
			}

			return macObj;
		}
		
		/// <summary>
		/// Name of the PRNG scheme (must be a member of PseudoRandomNumberGenerators).
		/// </summary>
		public string Name { get; internal set; }
		/// <summary>
		/// Name to show a user or for a detailed specification.
		/// </summary>
		public string DisplayName { get; internal set; }

		/// <summary>
		/// Whether the MAC operation may include salting.
		/// </summary>
		public bool SaltSupported { get; internal set; }
	}
	
	public static class BrainpoolECParameterStorage
	{
		private readonly static Dictionary<BrainpoolECFpCurves, Func<ECDomainParameters>> _ecParameters =
			new Dictionary<BrainpoolECFpCurves, Func<ECDomainParameters>>();
		
		static BrainpoolECParameterStorage () {
			_ecParameters.Add(BrainpoolECFpCurves.BrainpoolP160r1, () => GenerateDomainParameters(
				"E95E4A5F737059DC60DFC7AD95B3D8139515620F",
				"340E7BE2A280EB74E2BE61BADA745D97E8F7C300",
				"1E589A8595423412134FAA2DBDEC95C8D8675E58",
				"BED5AF16EA3F6A4F62938C4631EB5AF7BDBCDBC3",
				"1667CB477A1A8EC338F94741669C976316DA6321",
				"E95E4A5F737059DC60DF5991D45029409E60FC09"
				));
			
			_ecParameters.Add(BrainpoolECFpCurves.BrainpoolP192r1, () => GenerateDomainParameters(
				"C302F41D932A36CDA7A3463093D18DB78FCE476DE1A86297",
				"6A91174076B1E0E19C39C031FE8685C1CAE040E5C69A28EF",
				"469A28EF7C28CCA3DC721D044F4496BCCA7EF4146FBF25C9",
				"C0A0647EAAB6A48753B033C56CB0F0900A2F5C4853375FD6",
				"14B690866ABD5BB88B5F4828C1490002E6773FA2FA299B8F",
				"C302F41D932A36CDA7A3462F9E9E916B5BE8F1029AC4ACC1"
				));
			
			_ecParameters.Add(BrainpoolECFpCurves.BrainpoolP224r1, () => GenerateDomainParameters(
				"D7C134AA264366862A18302575D1D787B09F075797DA89F57EC8C0FF",
				"68A5E62CA9CE6C1C299803A6C1530B514E182AD8B0042A59CAD29F43",
				"2580F63CCFE44138870713B1A92369E33E2135D266DBB372386C400B",
				"0D9029AD2C7E5CF4340823B2A87DC68C9E4CE3174C1E6EFDEE12C07D",
				"58AA56F772C0726F24C6B89E4ECDAC24354B9E99CAA3F6D3761402CD",
				"D7C134AA264366862A18302575D0FB98D116BC4B6DDEBCA3A5A7939F"
				));
			
			_ecParameters.Add(BrainpoolECFpCurves.BrainpoolP256r1, () => GenerateDomainParameters(
				"A9FB57DBA1EEA9BC3E660A909D838D726E3BF623D52620282013481D1F6E5377",
				"7D5A0975FC2C3057EEF67530417AFFE7FB8055C126DC5C6CE94A4B44F330B5D9",
				"26DC5C6CE94A4B44F330B5D9BBD77CBF958416295CF7E1CE6BCCDC18FF8C07B6",
				"8BD2AEB9CB7E57CB2C4B482FFC81B7AFB9DE27E1E3BD23C23A4453BD9ACE3262",
				"547EF835C3DAC4FD97F8461A14611DC9C27745132DED8E545C1D54C72F046997",
				"A9FB57DBA1EEA9BC3E660A909D838D718C397AA3B561A6F7901E0E82974856A7"
				));
			
			_ecParameters.Add(BrainpoolECFpCurves.BrainpoolP320r1, () => GenerateDomainParameters(
				"D35E472036BC4FB7E13C785ED201E065F98FCFA6F6F40DEF4F92B9EC7893EC28FCD412B1F1B32E27",
				"3EE30B568FBAB0F883CCEBD46D3F3BB8A2A73513F5EB79DA66190EB085FFA9F492F375A97D860EB4",
				"520883949DFDBC42D3AD198640688A6FE13F41349554B49ACC31DCCD884539816F5EB4AC8FB1F1A6",
				"43BD7E9AFB53D8B85289BCC48EE5BFE6F20137D10A087EB6E7871E2A10A599C710AF8D0D39E20611",
				"14FDD05545EC1CC8AB4093247F77275E0743FFED117182EAA9C77877AAAC6AC7D35245D1692E8EE1",
				"D35E472036BC4FB7E13C785ED201E065F98FCFA5B68F12A32D482EC7EE8658E98691555B44C59311"
				));
			
			_ecParameters.Add(BrainpoolECFpCurves.BrainpoolP384r1, () => GenerateDomainParameters(
				"8CB91E82A3386D280F5D6F7E50E641DF152F7109ED5456B412B1DA197FB71123ACD3A729901D1A71874700133107EC53",
				"7BC382C63D8C150C3C72080ACE05AFA0C2BEA28E4FB22787139165EFBA91F90F8AA5814A503AD4EB04A8C7DD22CE2826",
				"4A8C7DD22CE28268B39B55416F0447C2FB77DE107DCD2A62E880EA53EEB62D57CB4390295DBC9943AB78696FA504C11",
				"1D1C64F068CF45FFA2A63A81B7C13F6B8847A3E77EF14FE3DB7FCAFE0CBD10E8E826E03436D646AAEF87B2E247D4AF1E",
				"8ABE1D7520F9C2A45CB1EB8E95CFD55262B70B29FEEC5864E19C054FF99129280E4646217791811142820341263C5315",
				"8CB91E82A3386D280F5D6F7E50E641DF152F7109ED5456B31F166E6CAC0425A7CF3AB6AF6B7FC3103B883202E9046565"
				));
			
			_ecParameters.Add(BrainpoolECFpCurves.BrainpoolP512r1, () => GenerateDomainParameters(
				"AADD9DB8DBE9C48B3FD4E6AE33C9FC07CB308DB3B3C9D20ED6639CCA703308717D4D9B009BC66842AECDA12AE6A380E62881FF2F2D82C68528AA6056583A48F3",
				"7830A3318B603B89E2327145AC234CC594CBDD8D3DF91610A83441CAEA9863BC2DED5D5AA8253AA10A2EF1C98B9AC8B57F1117A72BF2C7B9E7C1AC4D77FC94CA",
				"3DF91610A83441CAEA9863BC2DED5D5AA8253AA10A2EF1C98B9AC8B57F1117A72BF2C7B9E7C1AC4D77FC94CADC083E67984050B75EBAE5DD2809BD638016F723",
				"81AEE4BDD82ED9645A21322E9C4C6A9385ED9F70B5D916C1B43B62EEF4D0098EFF3B1F78E2D0D48D50D1687B93B97D5F7C6D5047406A5E688B352209BCB9F822",
				"7DDE385D566332ECC0EABFA9CF7822FDF209F70024A57B1AA000C55B881F8111B2DCDE494A5F485E5BCA4BD88A2763AED1CA2B2FA8F0540678CD1E0F3AD80892",
				"AADD9DB8DBE9C48B3FD4E6AE33C9FC07CB308DB3B3C9D20ED6639CCA70330870553E5C414CA92619418661197FAC10471DB1D381085DDADDB58796829CA90069"
				));
		}
		
		public static ECDomainParameters InstantiateImplementation (string name) {
			BrainpoolECFpCurves curve;
			try {
				name.ToEnum (out curve);
			} catch (Exception) {
				throw new NotSupportedException("Curve is unknown or otherwise unsupported.");
			}
			
			return InstantiateImplementation(curve);
		}
		
		public static ECDomainParameters InstantiateImplementation(BrainpoolECFpCurves curve) {
			return _ecParameters[curve]();
		}
		
		private static ECDomainParameters GenerateDomainParameters (string p, string A, string B, string x, string y, string q) {
			var curve = new FpCurve(new BigInteger(p, 16), new BigInteger(A, 16), new BigInteger(B, 16));
			return new ECDomainParameters(curve, curve.CreatePoint(new BigInteger(x, 16), new BigInteger(y, 16), false), new BigInteger(q, 16));
		}
	}
}
