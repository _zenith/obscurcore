using System;
using System.Collections.Generic;
using System.IO;

using ObscurCore.Extensions.Enumerations;
using ObscurCore.Packaging;
using ObscurCore.DTO;

namespace ObscurCore.Knowledge
{
	/// <summary>
	/// Stores a description of and means of instantiating a payload I/O module.
	/// </summary>
	public sealed class PayloadModuleDescription
	{
		private readonly static Dictionary<PayloadLayoutSchemes, Func<bool, Stream, List<IStreamBinding>, List<Func<Stream, DecoratingStream>>, 
			IPayloadLayoutConfiguration, StreamMux>> _moduleInstantiators = new Dictionary<PayloadLayoutSchemes, 
		Func<bool, Stream, List<IStreamBinding>, List<Func<Stream, DecoratingStream>>, IPayloadLayoutConfiguration, StreamMux>>();
		
		static PayloadModuleDescription () {
			_moduleInstantiators.Add(PayloadLayoutSchemes.Simple, (writing, multiplexedStream, streams, transforms, config) => 
			                         new SimpleMux(writing, multiplexedStream, streams, transforms, config));
			_moduleInstantiators.Add(PayloadLayoutSchemes.Frameshift, (writing, multiplexedStream, streams, transforms, config) => 
			                         new FrameshiftMux(writing, multiplexedStream, streams, transforms, config));
			_moduleInstantiators.Add(PayloadLayoutSchemes.Fabric, (writing, multiplexedStream, streams, transforms, config) => 
			                         new FabricMux(writing, multiplexedStream, streams, transforms, config));
		}
		
		/// <summary>
		/// Instantiates and returns a payload I/O module implementing the mode of operation that the
		/// instance this method was called from describes.
		/// </summary>
		/// <param name="config">Configuration of the module.</param>
		/// <returns>
		/// An module object deriving from IPayloadModule.
		/// </returns>
		public StreamMux InstantiateImplementation (bool writing, Stream multiplexedStream, List<IStreamBinding> streams, 
		                                            List<Func<Stream, DecoratingStream>> transforms, IPayloadLayoutConfiguration config)
		{
			PayloadLayoutSchemes payloadLayout;
			Name.ToEnum(out payloadLayout);
			return _moduleInstantiators[payloadLayout](writing, multiplexedStream, streams, transforms, config);
		}
		
		/// <summary>
		/// Name of the payload layout scheme (must be a member of PayloadLayoutSchemes).
		/// </summary>
		public string Name { get; internal set; }
		/// <summary>
		/// Name to show a user or for a detailed specification.
		/// </summary>
		public string DisplayName { get; internal set; }

		public override string ToString () {
			return string.Format ("Payload Layout Scheme & Stream Multiplexer: {0}", DisplayName);
		}
	}
}

