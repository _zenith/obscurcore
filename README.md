**ObscurCore**
==============

ObscurCore can be a variety of different things. It doesn�t force you to use it any one particular way. It can help you send secure messages and files on a mobile device. It can help you build a secure data transmission backbone in business IT. It can help you store files on a personal computer securely.

What it is mostly is an integrated framework for creating and consuming data with compression and encryption. The data can be raw, or packaged with other accompanying data, with associated internal structure if so desired (file names, relative directories and such, like that of compressed archive formats).

What sets ObscurCore apart is the ease of use, yet with a very high degree of control & customisability if desired, and the sheer variety and coverage of different encryption options available. It is designed to operate on scales from integration into mobile phone applications, or in the back end of enterprise software. ObscurCore takes advantage of multiprocessing capabilities wherever practical to do so, or is intended to with further development (not every situation will be using it currently - work is needed to extend the BouncyCastle API to utilise this, see below...).

Bouncy Castle for cryptography (mostly)
---------------------------------------
ObscurCore uses the C# [Bouncy Castle](http://www.bouncycastle.org/csharp/) cryptographic API for most security primitives, but hides the complexity of it all away. You don't need to be a security expert to use it - ObscurCore structures and curates access to cryptographic primitives to ensure that secure systems design is observed.
Other features are implemented independently.


Features
--------

Compression algorithms

- Bzip2
- Deflate
- LZ4 (super-fast, low resource use)

Block ciphers

- AES
- CAST5
- CAST6
- Camellia
- GOST 28147-89
- IDEA
- NOEKEON
- RC6
- Serpent
- Triple DES
- Twofish

Stream ciphers

- HC128
- HC256
- RC4
- Salsa20
- VMPC

Block cipher modes of operation

- CBC
- CTR/SIC
- CFB
- OFB
- CTS

Authenticated block cipher modes of operation *(AEAD)*

- GCM
- EAX
- *OCB* (in progress)
- *SIC* (in progress)

Padding schemes

- ISO/IEC 7816-4
- PKCS7
- TBC
- ANSI X.923

Key agreement schemes

- One-Pass Unified Model (UM1)

Key derivation functions

- **Scrypt**
- PBKDF2

Cryptographically secure pseudorandom number generators

- Salsa20 keystream
- SOSEMANUK keystream

Hash functions

- **BLAKE2B**
- **Keccak/SHA-3**
- RIPEMD family
- SHA 1-2 families
- Whirlpool


Streamlined & integrated
------------------------
It's easy to make ObscurCore fit your use case. It can produce Packages - akin to a compressed archive format, but designed from the bottom up for security, rather than it having been tacked on as an afterthought. A Package constitutes 3 things - sequentially and respectively, a ManifestHeader, a Manifest, and a Payload. The ManifestHeader describes how to read the Manifest, and the manifest the payload. A manifest functions in an indexing and metadata capacity.

Each item in a payload may have its own compression and cryptography configurations. The encryption keys may be either pre-established (and ObscurCore provides key confirmation facilities to verify this) or ephemeral keys included in the package for one-time use. All this metadata is stored in the manifest - only the item itself is stored in the payload section.

In the payload, items are concatenated together (in randomised order, with fixed/variable length stretches of random data before and after each item if desired, termed a *Frameshift* layout if included), or striped together in fixed/variable length stripes (which is termed a *Fabric*), again in randomised order.
CPRNGs are used for determining item ordering, and lengths of frameshift padding or item stripes. CSPRNGs currently implemented utillise the keystream from Salsa20 and SOSEMANUK stream ciphers, making them suitable for general cryptographic use.

The Manifest itself is encrypted. This provides for plausible deniability scenarios as well, because fake items can be included. Without access to a manifest's content description, it should not be possible to determine how many items are in any package, or what length they are. Even with this manifest information, it is not possible to discriminate sensitive from nonsensitive filler material.
Encryption of manifests can be performed with public key (asymmetric) methods or with symmetric keys. In either case it is ultimately a symmetric cipher which performs the encryption - in the former case, the PKC scheme is used to derive a symmetric key.

... Which take us on to a relevant subject:

Key Agreements
--------------
Key agreement may occur implicitly through deserialisation of a ManifestHeader - the originating party may specify that this key is to be retained for further communications, avoiding the considerable computational overhead of forming an ephemeral agreement for each package (typical use case for a messaging session), or that it is to be used only in the context of that individual package instance (may be useful for larger or more important communications). Alternatively - or in addition to - a KeyAgreementConfiguration may be included in a Payload as an otherwise normal item, indicating that it is to be used in later communications with that party.

Agreements are transferred in the form of a serialised KeyAgreementConfiguration object as part of a ManifestHeader or as aforementioned, an item in a Payload.
These objects comprise instructions on how to agree on a shared secret, or an existing one to derive from, and configuration data for a KDF to further derive the shared secret. KDFs available are as per those available for use in PayloadItems.

Use of a public-key cryptographic scheme allows key agreement with a remote party without auxillary communication through a side channel. Currently, the only scheme supported is the NIST-recommended One-Pass Unified Model (UM1), a 1-pass Diffie-Hellman-type scheme utillising elliptic curves with cofactor multiplication as its basis. In future, FH-ECMQV may be implemented, depending on whether this is requested or not. MQV was not included due to concerns with potential seurity vulnerabilities, despite its better efficiency.

A KeyAgreementConfiguration may include no new key or method of agreeing on one, but rather specify a different action to perform to an existing key. These options include dissociating the key (for convenience or because suspected vulnerability), verifying it (utilising a KeyConfirmationConfiguration object), and 'advancing' it (essentially, further deriving an existing key according to some deterministic scheme). The default action is Association.

Serialisation
--------------------------------------
 is used for communication of the Data Transfer Objects comprising a Package (in byte order, a ManifestHeader prefacing a Manifest). Any object that is part of a package may be serialised seperately if desired for your use case.

Google's excellent [Protocol Buffers](https://developers.google.com/protocol-buffers/) serialisation format is used in the form of the also excellent .NET implementation, [protobuf-net](http://code.google.com/p/protobuf-net/). This is a highly efficient, high-performance solution, with similar capabilities to XML or JSON formats, but much faster operation and much smaller output size.
Performance is further increased through use of a precompiled protobuf-net serialiser assembly, avoiding Reflection costs.

Stream interface
----------------
The compression and symmetric encryption functionality available in ObscurCore is exposed through decorating streams (this is a great programming pattern), so you can just feed in bytes to a black box and get what you want out the other side.

The relevant objects are compression with CompressoStream, and cryptography (encryption) with CryptoStream (not the .NET CryptoStream - similar concept, but extensive).
They are used just like any other Stream when initialised. Closing the stream signifies the end of I/O operations and finalises the internal state of the used algorithms/ciphers, emitting final blocks and the like.

Clean code
----------
The core streams (Compresso-, Crypto-) are initialised with their own types of configuration objects, which are designed to be serialised for transmission. The same applies for the payload packaging modules (but they are not exposed as streams). This is how they are included in the package header, for example.
This prevents the situation you may be familiar with - having to feed in many complex parameters, cluttering up code in the process. The same object used for creation is used for remote consumption without modification.

Modularity
----------
All the major components can be tied together, or used separately as required. If you have no need for producing packages, simply use the *CryptoStream* or *CompressoStream* objects as you will.
You can even use the Packaging objects (e.g *FabricPayloadModule*, or others) if you find a use for them like that.

If you are using ObscurCore for securing communications between software - such as server-client relationships, or P2P - or storing data, such as in saved application state or data files, and your use case does not involve highly intensive I/O and many, short messages, it is recommended that you use the Package use model. You can send/store multiple messages/objects/files at a time this way, potentially each with its own configuration making different data available to different users, and even change configuration on the fly with no disruption of service.

StratCom
--------
a.k.a. *Strategic Command*. This is the name of the integrated data pipeline that automatically strings together compression, encryption, and packaging into a single operation. It is also intended for use in situations where many packages are being created and consumed simultaneously (such as if it were a key part of a application backend serving many requests). StratCom is designed to knit into its utilising application to prevent unnecessary service duplication. Simply implement one or two interfaces to show StratCom how to query your application's state.