using System;
using System.IO;

using ObscurCore.DTO;
using ObscurCore.Extensions.Generic;
using ObscurCore.Extensions.Streams;

namespace ObscurCore.Compression
{
	/// <summary>
	/// Configuration for CompressoStream compressing/decompressing streams.
	/// </summary>
	[Version(1,0)]
	public static class CompressionConfigurationFactory
	{		
		public static CompressionConfiguration Create (CompressionAlgorithms algorithm)
		{
			var algoConfig = new byte[0];
			switch (algorithm) {
			case CompressionAlgorithms.Bzip2:
				algoConfig = Bzip2ConfigurationUtility.Write(Bzip2ConfigurationUtility.DefaultBlockSize);
				break;
			case CompressionAlgorithms.Deflate:
				algoConfig = DeflateConfigurationUtility.Write(DeflateConfigurationUtility.DefaultCompressionLevel);
				break;
			case CompressionAlgorithms.LZ4:
				algoConfig = null;
				break;
			}
            if(algorithm != CompressionAlgorithms.LZ4 && algoConfig.Length == 0) throw new ArgumentException();
			
			return new CompressionConfiguration {
                AlgorithmName = algorithm.ToString(),
				AlgorithmConfiguration = algoConfig
			};
		}
	}
	
	public static class Bzip2ConfigurationUtility
	{
		public static readonly int DefaultBlockSize = Bzip2.BZip2.MaxBlockSize;
		
		/// <summary>
		/// Reads an Bzip2 algorithm configuration from shorthand byte array format, and outputs the 
		/// 'blockSize' parameter to external variable field.
		/// </summary>
		/// <param name='blockSize'>
		/// Size of the block written/read in Bzip2 data encoding.
		/// </param>
		public static void Read(byte[] config, out int blockSize) {
			if(config.Length != 1) throw new ArgumentException("Configuration is invalid, not 1 byte.");
			blockSize = config[0];
            if(!blockSize.IsBetween(Bzip2.BZip2.MinBlockSize, Bzip2.BZip2.MaxBlockSize)) throw new ArgumentOutOfRangeException("blockSize");
		}
		
		/// <summary>
		/// Writes an Bzip2 algorithm configuration in shorthand byte array format.
		/// </summary>
		/// <param name='blockSize'>
		/// Size of the block written/read in Bzip2 data encoding.
		/// </param>
		public static byte[] Write(int blockSize) {
            if (!blockSize.IsBetween(Bzip2.BZip2.MinBlockSize, Bzip2.BZip2.MaxBlockSize)) throw new ArgumentOutOfRangeException("blockSize");
            return new[] { (byte) blockSize };
		}
	}
	
	public static class DeflateConfigurationUtility
	{
		public static readonly Deflate.CompressionLevel DefaultCompressionLevel = Deflate.CompressionLevel.Default;
		
		/// <summary>
		/// Reads an Deflate algorithm configuration from shorthand byte array format, and outputs the 
		/// 'blockSize' parameter to external variable field.
		/// </summary>
        /// <param name='deflateLevel'>
		/// Level of Deflate compression.
		/// </param>
		public static void Read(byte[] config, out Deflate.CompressionLevel deflateLevel) {
			if(config.Length != 1) throw new ArgumentException("Configuration is invalid, not 1 byte.");
			deflateLevel = (Deflate.CompressionLevel) Enum.ToObject(typeof(Deflate.CompressionLevel), config[0]);
		}
		
		/// <summary>
		/// Writes an Deflate algorithm configuration in shorthand byte array format.
		/// </summary>
        /// <param name='deflateLevel'>
		/// Level of Deflate compression.
		/// </param>
		public static byte[] Write(Deflate.CompressionLevel deflateLevel) {
			var config = new byte[1];
			config[0] = (byte) deflateLevel;
		    return config;
		}
	}
}

