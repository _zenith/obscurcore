using System;
using System.IO;
using ObscurCore.Compression.Bzip2;
using ObscurCore.Compression.Deflate;
using ObscurCore.Compression.LZ4;
using ObscurCore.Extensions.Generic;
using ObscurCore.Extensions.Enumerations;
using ObscurCore.DTO;

namespace ObscurCore.Compression
{
	/// <summary>
	/// Decorating stream encapsulating and implementing compression/decompression operations transparently.
	/// </summary>
	[Version(1,0)]
	public class CompressoStream : DecoratingStream
	{
	    private bool _eof;
		/// <summary>
		/// What mode is active - compression or decompression?
		/// </summary>
		public bool Compressing { get; private set; }

        /// <summary>General-purpose constructor for all compressing/decompressing streams.</summary>
        /// <param name="target">Stream to be written/read to/from.</param>
        /// <param name="isCompressing">Specifies whether the stream is for writing (compression) or reading (decompression).</param>
        /// <param name="leaveOpen">Set to <c>false</c> to also close the base stream when closing, or vice-versa.</param>
        /// <param name="config">Configuration object describing how to set up the internal compression/decompression streams and associated services.</param>
		public CompressoStream (Stream target, bool isCompressing, bool leaveOpen, ICompressionConfiguration config) 
			: base(isCompressing, !leaveOpen)
		{
			Compressing = isCompressing;
			CompressionAlgorithms algorithm;
            try { config.AlgorithmName.ToEnum(out algorithm); }
            catch (ArgumentException)
            {
                throw new NotSupportedException("Algorithm type specified is unknown.");
            }

			switch (algorithm) {
			case CompressionAlgorithms.Bzip2:
			    int blockSizeBzip2;
                try { Bzip2ConfigurationUtility.Read(config.AlgorithmConfiguration, out blockSizeBzip2); } 
                catch (Exception)
                {
                    throw new InvalidDataException("Error reading Bzip2 algorithm block size.");
                }
                if (!blockSizeBzip2.IsBetween(BZip2.MinBlockSize, BZip2.MaxBlockSize)) 
                    throw new NotSupportedException("Algorithm configuration specifies the use of an unsupported block size.");

                #if (MOBILE)
                if(IsCompressing) BaseStream = new BZip2OutputStream(target, blockSize, leaveOpen);
                    else BaseStream = new BZip2InputStream(target, leaveOpen);
			    #else
                if (Compressing) BoundStream = new ParallelBZip2OutputStream(target, blockSizeBzip2, leaveOpen);
                    else BoundStream = new BZip2InputStream(target, leaveOpen);
				#endif
				break;
			case CompressionAlgorithms.Deflate:
				Deflate.CompressionLevel deflateLevel;
			    try {  DeflateConfigurationUtility.Read(config.AlgorithmConfiguration, out deflateLevel); } 
                catch (Exception)
			    {
                    throw new InvalidDataException("Error reading Deflate algorithm compression level.");
			    }

				#if (MOBILE)
                if(IsCompressing) BaseStream = new DeflateStream(target, CompressionMode.Compress, leaveOpen);
					else BaseStream = new DeflateStream(target, CompressionMode.Decompress, leaveOpen);
				#else
                if (Compressing) BoundStream = new ParallelDeflateOutputStream(target, deflateLevel, CompressionStrategy.Default, leaveOpen);
                    else BoundStream = new DeflateStream(target, CompressionMode.Decompress, leaveOpen);
				#endif
				
				break;
			case CompressionAlgorithms.LZ4:
                BoundStream = new LZ4Stream(target, Compressing, !leaveOpen);
				break;
			default:
				throw new NotImplementedException("Algorithm is not implemented yet.");
			}
		}
		
		public override int ReadByte() {
            if (Compressing) throw new NotSupportedException("Stream is configured for compression, and so may only be written to.");
            if (_eof) return -1;
            var returnedByte = base.ReadByte();
            if(returnedByte == -1) {
                // Return processing counts to what they were before -1'ing
                BytesIn++;
                BytesOut++;
                _eof = true;
                return -1;
            }
		    return returnedByte;
		}
		
		public override int Read(byte[] buffer, int offset, int count) {
            if (Compressing) throw new NotSupportedException("Stream is configured for compression, and so may only be written to.");
            if (_eof) return 0;
            var readBytes = base.Read(buffer, offset, count);
            if ((readBytes == -1 && BoundStream is BZip2InputStream)) {
                // Return processing counts to what they were before -1'ing
                BytesIn++;
                BytesOut++;
                return 0;
            }
			return readBytes;
		}
		
		// put close and flush in here

		public override void Close() {
            BoundStream.Close(); // TODO: More extensive close behaviour.
		}

	}
}

