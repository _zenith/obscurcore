using System;
using System.IO;

using ObscurCore.Extensions.Generic;
using ObscurCore.Extensions.Streams;

namespace ObscurCore.Compression.LZ4
{
    /// <summary>
    /// High performance decorating stream implementing LZ4 compression/decompression algorithm.
    /// </summary>
    public sealed class LZ4Stream : Stream
    {
        /*******************************************************************************
         *              Description of LZ4 Streaming Format Version 1  
         * 
         *  "LZ4SFv1>" in UTF-8 format bytes is at the start of the stream.
         * 	An arbitrary number of blocks follow this, with the following byte format:
         * 
         *  [uLengthInt32][cLengthInt32]{block:cLength} ...
         * 
         * 	(replace [...] {...} fields with just the data specified)
         * 
         *  uLengthInt = Length of data in block when it is uncompressed,
         *  expressed as a 32-bit signed int (4 bytes).
         * 
         *  cLengthInt = Length of data in block when it is compressed
         *  (length of the block), expressed as a 32-bit signed int (4 bytes).
         * 
         *  block = Data in any format, but must be of the length specified prior.
         * 
         *  EOS is specified by sign of length descriptor preceding the last block.
         *  When read, it indicates reading should cease after finishing the block.
         * 
         *  Length sign is negative to specify EOS state, and otherwise positive.
         *  
         ********************************************************************************/

        #region Public properties
        /// <summary>
        /// Stream being written to (compression) or read from (decompression).
        /// </summary>
        public Stream StreamBinding { get; private set; }

        /// <summary>
        /// Total number of bytes written or read in compressed format.
        /// </summary>
        public long TotalCompressed { get; private set; }

        /// <summary>
        /// Total number of bytes written or read in uncompressed format.
        /// </summary>
        public long TotalUncompressed { get; private set; }

        /// <summary>
        /// Operation mode of stream. Only writes may be performed if compressing, and vice-versa.
        /// </summary>
        /// <value><c>true</c> if compressing; otherwise, <c>false</c>.</value>
        public bool Compressing { get; private set; }
        #endregion

        public const int MinimumBlockSize = 512, MaximumBlockSize = 2097152; // half KB, 2 MB
        public const int DefaultWriteThreshold = 65536; // 64 KB

        #region Private fields and constants
        private readonly CyclicByteBuffer _cyclicBuffer;
        private readonly ILZ4Compressor _compressor;
        private readonly ILZ4Decompressor _decompressor;
        private readonly int _writeTheshold;
        private readonly bool _bindStreamLifecycle;
        private bool _firstBlock = true, _eosReached;
        // Constants
        private const float OverflowTolerance = 0.2f; // 20 %
        // LZ4 Variable Block Length Streaming Format v1
        private const string HeaderString = "LZ4SFv1>";
        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="LZ4Stream"/> class.
        /// </summary>
        /// <param name="binding">Binding.</param>
        /// <param name="bindLifecycle">If set to <c>true</c>, bound stream will be closed when the LZ4Stream closes.</param>
        /// <param name="writeThreshold">
        /// Minimum size required to write a 'block'. 
        /// Tuning parameter. Inactive if decompressing.
        /// </param>
        public LZ4Stream (Stream binding, bool compressing, bool bindLifecycle, int writeThreshold = DefaultWriteThreshold) {
            StreamBinding = binding;
            Compressing = compressing;
            _bindStreamLifecycle = bindLifecycle;

            //if (!writeThreshold.IsBetween(MinimumBlockSize, (int) (MaximumBlockSize + (MaximumBlockSize * _overflowTolerance))))
                //throw new ArgumentOutOfRangeException("writeThreshold", writeThreshold, "Write threshold not within supported range.");
            _writeTheshold = writeThreshold;
            _cyclicBuffer = new CyclicByteBuffer((int) (_writeTheshold + (_writeTheshold * OverflowTolerance)));

            if (Compressing) _compressor = LZ4CompressorFactory.CreateNew();
            else _decompressor = LZ4DecompressorFactory.CreateNew();
        }

        public override int Read (byte[] buffer, int offset, int count) {
            if (Compressing)
                throw new NotSupportedException("Cannot read - stream is in compression mode. Writing-only.");
            if (count == 0) return 0;

            if (_firstBlock) {
                string header;
                StreamBinding.ReadPrimitive(out header);
                if (!header.Equals(HeaderString)) throw new InvalidDataException("Expected header missing or malformed. Not a valid LZ4 stream.");
                _firstBlock = false;
            }

            if (_cyclicBuffer.Length >= count) {
                _cyclicBuffer.Take(buffer, offset, count);
                return count;
            }

            var returned = 0;

            if (_cyclicBuffer.Length > 0) {
                var cylBufLen = _cyclicBuffer.Length;
                _cyclicBuffer.Take(buffer, offset, cylBufLen);
                offset += cylBufLen;
                count -= cylBufLen;
                returned += cylBufLen;
            }

            if (_eosReached) return returned;

            byte[] block;
            bool eos;
            ReadUncompressed(out block, out eos);
            if (eos) _eosReached = true;

            if (block.Length == 0) return returned;
            if (block.Length <= count) {
                Buffer.BlockCopy(block, 0, buffer, offset, block.Length);
                returned += block.Length;
            } else {
                Buffer.BlockCopy(block, 0, buffer, offset, count);
                returned += count;
                _cyclicBuffer.Put(block, count, block.Length - count);
            }

            return returned;
        }

        /// <summary>
        /// Reads and decompresses a block from the bound stream.
        /// </summary>
        /// <returns>Length of block of compressed data that was read.</returns>
        /// <param name="block">Decompressed data block output.</param>
        /// <param name="eos">If set to <c>true</c>, this block is the end of the LZ4 stream.</param>
        private void ReadUncompressed (out byte[] block, out bool eos) {
            bool eosFlag1, eosFlag2;
            int lenExpanded;
            StreamBinding.ReadPrimitive(out lenExpanded);
            eosFlag1 = Math.Sign(lenExpanded) < 0;
            lenExpanded = Math.Abs(lenExpanded);

            if (lenExpanded == 0) {
                int eosCheck;
                StreamBinding.ReadPrimitive(out eosCheck);
                if (eosCheck != -1)
                    throw new InvalidDataException("Stream EOS marker malformed. Final block had zero-length property but check failed.");
                block = new byte[0];
                eos = true;
                return;
            }

            if (lenExpanded > MaximumBlockSize)
                throw new InvalidDataException("Block length descriptor malformed. Higher than allowed maximum - does not conform to specification.");

            byte[] compressedBlock;
            StreamBinding.ReadPrimitiveMeta(out compressedBlock, out eosFlag2);

            if (eosFlag1 != eosFlag2)
                throw new InvalidDataException("Stream EOS marker malformed. Block length descriptor sign inconsistent - state cannot be determined.");
            eos = eosFlag1;

            block = new byte[lenExpanded];
            _decompressor.DecompressKnownSize(compressedBlock, block, lenExpanded);

            TotalCompressed += compressedBlock.Length;
            TotalUncompressed += lenExpanded;
        }

        public override int ReadByte () {
            if (Compressing)
                throw new NotSupportedException("Cannot read - stream is in compression mode. Writing-only.");

            if (_cyclicBuffer.Length > 0) return _cyclicBuffer.Take();
            if (_eosReached) return -1;

            byte[] block;
            bool eos;
            ReadUncompressed(out block, out eos);
            if (eos) _eosReached = true;
            return _cyclicBuffer.Take();
        }

        public override void Write (byte[] buffer, int offset, int count) {
            if (!Compressing)
                throw new NotSupportedException("Cannot write - stream is in decompression mode. Reading-only.");
            if (count == 0) return;

            if (_firstBlock) {
                StreamBinding.WritePrimitive(HeaderString);
                _firstBlock = false;
            }

            int cylBufLen = _cyclicBuffer.Length;
            int sum = cylBufLen + count;
            if (sum >= _writeTheshold) {
                var block = new byte[sum];
                _cyclicBuffer.Take(block, 0, cylBufLen);
                Buffer.BlockCopy(buffer, offset, block, cylBufLen, count);
                WriteCompressed(block, false);
            } else _cyclicBuffer.Put(buffer, offset, count);
        }

        /// <summary>
        /// Compresses and writes a block to the bound stream.
        /// </summary>
        /// <param name="block">Uncompressed block of data to be compressed and written.</param>
        /// <param name="eos">If set to <c>true</c>, this block will be the end of the LZ4 stream.</param>
        private void WriteCompressed (byte[] block, bool eos) {
            // Write int for uncompressed block length with negative sign if end of stream, otherwise positive
            StreamBinding.WritePrimitive(eos ? -block.Length : block.Length);
            var output = new byte[_compressor.CalculateMaxCompressedLength(block.Length)];
            var compressedLength = _compressor.Compress(block, output);
            StreamBinding.WritePrimitiveMeta(output, 0, compressedLength, eos);

            TotalCompressed += compressedLength;
            TotalUncompressed += block.Length;
        }

        public override void WriteByte (byte value) {
            if (!Compressing)
                throw new NotSupportedException("Cannot write - stream is in decompression mode. Reading-only.");

            var sum = _cyclicBuffer.Length + 1;
            if (sum >= _writeTheshold) {
                var block = new byte[sum];
                _cyclicBuffer.Take(block, 0, sum - 1);
                block[sum - 1] = value;
                WriteCompressed(block, false);
            } else {
                _cyclicBuffer.Put(value);
            }
        }

        public override void Close () {
            if (Compressing) Flush(true, false);
            if (_bindStreamLifecycle) StreamBinding.Close();
        }

        /// <summary>
        /// Clears all buffers for this stream and causes any buffered data to be written
        /// to the underlying device. You shouldn't have to use this - use Close instead!
        /// </summary>
        public override void Flush () {
            Flush(false, true);
        }

        private void Flush (bool final, bool underlying) {
            if (Compressing) {
                if (_cyclicBuffer.Length > 0) {
                    var block = _cyclicBuffer.ToArray();
                    WriteCompressed(block, final);
                } else {
                    StreamBinding.WritePrimitive((int) 0); // EOS marker for zero-length block
                    StreamBinding.WritePrimitive((int) -1); // EOS verification
                }
            }
            if (underlying) StreamBinding.Flush();
        }

        /// <summary>Compresses a byte array with LZ4 algorithm.</summary>
        /// <returns>Compressed form of the supplied data as byte array, with no header or other data.</returns>
        public static byte[] CompressData (byte[] input) {
            var compressor = LZ4CompressorFactory.CreateNew();
            var outputRaw = new byte[compressor.CalculateMaxCompressedLength(input.Length)];
            var outSize = compressor.Compress(input, outputRaw);
            var output = new byte[outSize];
            Buffer.BlockCopy(outputRaw, 0, output, 0, outSize);
            return output;
        }

        /// <summary>Decompresses a raw byte array of LZ4-compressed data (do not supply stream format LZ4SFv1 data).</summary>
        /// <returns>Decompressed form of the supplied data as byte array.</returns>
        public static byte[] DecompressData (byte[] input, int? outSize = null) {
            var decompressor = LZ4DecompressorFactory.CreateNew();
            var outputRaw = new byte[outSize ?? input.Length]; ;
            int actualOutSize;
            if (outSize != null) {
                actualOutSize = decompressor.DecompressKnownSize(input, outputRaw, outSize ?? input.Length);
                if (actualOutSize != outSize)
                    throw new ArgumentException("Output size does not match supplied size.");
            } else {
                actualOutSize = decompressor.Decompress(input, outputRaw, input.Length);
            }
            var output = new byte[actualOutSize];
            Buffer.BlockCopy(outputRaw, 0, output, 0, actualOutSize);
            return output;
        }

        public override bool CanRead {
            get { return !Compressing && StreamBinding.CanRead; }
        }

        public override bool CanWrite {
            get { return Compressing && StreamBinding.CanWrite; }
        }

        public override bool CanSeek {
            get { return false; }
        }

        public sealed override long Length {
            get { throw new NotSupportedException(); }
        }

        public sealed override long Position {
            get { throw new NotSupportedException(); }
            set { throw new NotSupportedException(); }
        }

        public sealed override long Seek (long offset, SeekOrigin origin) {
            throw new NotSupportedException();
        }

        public sealed override void SetLength (long length) {
            if (!Compressing) throw new NotSupportedException();
            if (StreamBinding is FileStream)
                StreamBinding.SetLength(length);
        }
    }
}
