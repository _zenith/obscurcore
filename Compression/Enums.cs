using System;

namespace ObscurCore.Compression
{
	public enum CompressionAlgorithms
	{
		/// <summary>
		/// Ye olde LZ77-huffman coding algorithm, popularised through zlib and friends.
		/// </summary>
		Deflate,
		/// <summary>
		/// Burrows-Wheeler-Transform (BWT) based. Good compression, but somewhat slow.
		/// </summary>
		Bzip2,
		/// <summary>
		/// Very fast compression and even faster decompression, low memory use, but mediocre ratios.
		/// </summary>
		LZ4
	}
}

