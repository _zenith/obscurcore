﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ObscurCore.Cryptography;

namespace ObscurCore.Tests.Cryptography.BlockCiphers
{
    class Twofish : BlockCipherTestBase
    {
        public Twofish ()
            : base(SymmetricBlockCiphers.Twofish, 128, 256) {
        }
    }
}
