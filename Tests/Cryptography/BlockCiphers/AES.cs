﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using ObscurCore.Cryptography;

namespace ObscurCore.Tests.Cryptography.BlockCiphers
{
    public class AES : BlockCipherTestBase
    {
        public AES() : base(SymmetricBlockCiphers.AES, 128, 128) { }
    }
}
