﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ObscurCore.Cryptography;

namespace ObscurCore.Tests.Cryptography.BlockCiphers
{
    class RC6 : BlockCipherTestBase
    {
        public RC6 ()
            : base(SymmetricBlockCiphers.RC6, 128, 256) {
        }
    }
}
