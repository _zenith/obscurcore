﻿using System;
using ObscurCore.Cryptography;

using NUnit.Framework;

namespace ObscurCore.Tests.Cryptography.StreamCiphers
{
    class StreamCipherTests : CryptoTestBase
    {
        public override void AuxTestFixtureInit () {
            SetRandomFixtureKey(256);
        }

        [Test]
        public void HC128 () {
            var config = new StreamCipherConfiguration(SymmetricStreamCiphers.HC128) { IV = CreateRandomKey(128) };
            RunEqualityTest(config, CreateRandomKey(128));
        }

        [Test]
        public void HC256 () {
            var config = new StreamCipherConfiguration(SymmetricStreamCiphers.HC256) { IV = CreateRandomKey(256) };
            RunEqualityTest(config);
        }

        [Test]
        public void Rabbit () {
            var config = new StreamCipherConfiguration(SymmetricStreamCiphers.Rabbit) { IV = CreateRandomKey(64) };
            RunEqualityTest(config, CreateRandomKey(128));
        }

        [Test]
        public void RC4_96 () {
            var config = new StreamCipherConfiguration(SymmetricStreamCiphers.RC4, 96) { IV = CreateRandomKey(96) };
            RunEqualityTest(config, CreateRandomKey(96));
        }

        [Test]
        public void RC4_128 () {
            var config = new StreamCipherConfiguration(SymmetricStreamCiphers.RC4, 128) { IV = CreateRandomKey(128) };
            RunEqualityTest(config, CreateRandomKey(128));
        }

        [Test]
        public void Salsa20_256 () {
            // FIXME: Fix this IV problem properly in StreamCipherConfiguration constructor where it's first assigned. No dirty hacks allowed ^__^ .
            // Probably put in a AllowableIVSizes section in the Athena system too so this process is automated...
            // ... Other ciphers will probably have this issue too!
            var config = new StreamCipherConfiguration(SymmetricStreamCiphers.Salsa20) { IV = CreateRandomKey(64) };
            RunEqualityTest(config);
        }

        [Test]
        public void SOSEMANUK () {
            var config = new StreamCipherConfiguration(SymmetricStreamCiphers.SOSEMANUK) { IV = CreateRandomKey(128) };
            RunEqualityTest(config);
        }

        [Test]
        public void VMPC_256 () {
            var config = new StreamCipherConfiguration(SymmetricStreamCiphers.VMPC) { IV = CreateRandomKey(256) };
            RunEqualityTest(config);
        }
    }
}
