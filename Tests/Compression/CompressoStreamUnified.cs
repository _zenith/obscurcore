﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

using NUnit.Framework;
using ObscurCore.Compression;
using ObscurCore.DTO;

namespace ObscurCore.Tests.Compression
{
    class CompressoStreamUnified : IOTestBase
    {
        [Test]
        public void Unified_Bzip2 () {
            var config = new CompressionConfiguration() {
                AlgorithmName = CompressionAlgorithms.Bzip2.ToString(),
                AlgorithmConfiguration = Bzip2ConfigurationUtility.Write(9)
            };

            string output;
            Assert.IsTrue(UnifiedTest(config, out output));
            Assert.Pass(output);
        }


		[Test]
		public void Unified_LZ4() {
			var config = new CompressionConfiguration () { AlgorithmName = CompressionAlgorithms.LZ4.ToString() };

            string output;
            Assert.IsTrue(UnifiedTest(config, out output));
            Assert.Pass(output);
        }

		[Test]
		public void Unified_Deflate() {
			var config = new CompressionConfiguration () { 
				AlgorithmName = CompressionAlgorithms.Deflate.ToString(), 
				AlgorithmConfiguration = DeflateConfigurationUtility.Write(ObscurCore.Compression.Deflate.CompressionLevel.BestCompression)
			};

		    string output;
			Assert.IsTrue(UnifiedTest(config, out output));
            Assert.Pass(output);
		}

        bool UnifiedTest(CompressionConfiguration config, out string output) {
            var compressed = new MemoryStream();
            var sw = new Stopwatch();

            sw.Start();
            using (var cs = new CompressoStream(compressed, true, true, config)) {
                PdfStream.CopyTo(cs, GetBufferSize());
            }
            sw.Stop();

            long compTime = sw.ElapsedMilliseconds;
            double compSpeed = ((double) PdfStream.Length / 1048576) / sw.Elapsed.TotalSeconds;

            var decompressed = new MemoryStream();
            compressed.Seek(0, SeekOrigin.Begin);

            sw.Reset();
            sw.Start();
            using (var cs = new CompressoStream(compressed, false, true, config)) {
                cs.CopyTo(decompressed, GetBufferSize());
            }
            sw.Stop();

            long decompTime = sw.ElapsedMilliseconds;
            double decompSpeed = ((double) PdfStream.Length / 1048576) / sw.Elapsed.TotalSeconds;

            output = String.Format("{0:N0} ms ({1:N2} MB/s) : {2:N0} ms ({3:N2} MB/s) [{4:P}]", compTime, compSpeed, decompTime, decompSpeed, ((float) PdfStream.Length / (float) compressed.Length) - 1.0f);
            
            return decompressed.ToArray().SequenceEqual(PdfStream.ToArray());
        }
    }
}
