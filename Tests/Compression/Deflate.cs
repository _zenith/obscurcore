﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NUnit.Framework;
using ObscurCore.Compression.Deflate;

namespace ObscurCore.Tests.Compression
{
    [TestFixture]
    class Deflate : IOTestBase
    {
        [Test(Description = "Compression and decompression using Deflate stream API.")]
        public void DeflateStream () {
            var compressed = new MemoryStream();
            using (var ds = new DeflateStream(compressed, CompressionMode.Compress, CompressionLevel.Default, true)) {
                PdfStream.CopyTo(ds, GetBufferSize());
            }

            var decompressed = new MemoryStream();
            compressed.Seek(0, SeekOrigin.Begin);
            using (var ds = new DeflateStream(compressed, CompressionMode.Decompress, CompressionLevel.Default, true)) {
                ds.CopyTo(decompressed, GetBufferSize());
            }

            Assert.IsTrue(decompressed.ToArray().SequenceEqual(PdfStream.ToArray()));
            Assert.Pass("Compressed size: {0} ({1:P} size reduction) Uncompressed size: {2}", compressed.Length, ((float) PdfStream.Length / (float) compressed.Length) - 1.0f, PdfStream.Length);
        }

        [Test(Description = "Compression and decompression using Deflate stream API. Parallelism enabled for compression only.")]
        public void DeflateStream_Parallel () {
            var compressed = new MemoryStream();
            using (var ds = new ParallelDeflateOutputStream(compressed, CompressionLevel.Default, true)) {
                PdfStream.CopyTo(ds, GetBufferSize());
            }

            var decompressed = new MemoryStream();
            compressed.Seek(0, SeekOrigin.Begin);
            using (var ds = new DeflateStream(compressed, CompressionMode.Decompress, CompressionLevel.Default, true)) {
                ds.CopyTo(decompressed, GetBufferSize());
            }

            Assert.IsTrue(decompressed.ToArray().SequenceEqual(PdfStream.ToArray()));
            Assert.Pass("Compressed size: {0} ({1:P} size reduction) Uncompressed size: {2}", compressed.Length, ((float) PdfStream.Length / (float) compressed.Length) - 1.0f, PdfStream.Length);
        }
    }
}
