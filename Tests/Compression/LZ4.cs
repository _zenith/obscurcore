﻿using System;
using System.IO;
using System.Linq;
using NUnit.Framework;
using ObscurCore.Compression.LZ4;

namespace ObscurCore.Tests.Compression
{
    public class LZ4 : IOTestBase
    {
        [Test]
        public void LZ4Stream () {
            var compressed = new MemoryStream();
            using (var lz4 = new LZ4Stream(compressed, true, false)) {
                PdfStream.CopyTo(lz4, GetBufferSize());
            }

            var decompressed = new MemoryStream();
            compressed.Seek(0, SeekOrigin.Begin);
            using (var lz4 = new LZ4Stream(compressed, false, false)) {
                lz4.CopyTo(decompressed, GetBufferSize());
            }

            Assert.IsTrue(decompressed.ToArray().SequenceEqual(PdfStream.ToArray()));
            Assert.Pass("Compressed size: {0} ({1:P} size reduction) Uncompressed size: {2}", compressed.Length, ((float) PdfStream.Length / (float) compressed.Length) - 1.0f, PdfStream.Length);
        }
    }
}
