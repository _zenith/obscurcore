﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NUnit.Framework;
using ObscurCore.Compression.Bzip2;

namespace ObscurCore.Tests.Compression
{
    class Bzip2 : IOTestBase
    {
        [Test]
        public void Bzip2Stream () {
            var compressed = new MemoryStream();
            using (var bz2 = new BZip2OutputStream(compressed, true)) {
                PdfStream.CopyTo(bz2, GetBufferSize());
            }

            var decompressed = new MemoryStream();
            compressed.Seek(0, SeekOrigin.Begin);
            compressed.Position = 0;
            using (var bz2 = new BZip2InputStream(compressed, true)) {
                // FIXME: Why can't we use the CopyTo command like the other streams!? That is very dodgy.
                var buffer = new byte[GetBufferSize()];
                int n;
                while ((n = bz2.Read(buffer, 0, buffer.Length)) > 0) {
                    decompressed.Write(buffer, 0, n);
                }
            }

            Assert.IsTrue(decompressed.ToArray().SequenceEqual(PdfStream.ToArray()));
            Assert.Pass("Compressed size: {0} ({1:P} size reduction) Uncompressed size: {2}", compressed.Length, ((float) PdfStream.Length / (float) compressed.Length) - 1.0f, PdfStream.Length);
        }

        [Test]
        public void Bzip2Stream_Parallel () {
            var compressed = new MemoryStream();
            using (var bz2 = new ParallelBZip2OutputStream(compressed, true)) {
                PdfStream.CopyTo(bz2, GetBufferSize());
            }

            var decompressed = new MemoryStream();
            compressed.Seek(0, SeekOrigin.Begin);
            compressed.Position = 0;
            using (var bz2 = new BZip2InputStream(compressed, true)) {
                // FIXME: Why can't we use the CopyTo command like the other streams!? That is very dodgy.
                var buffer = new byte[GetBufferSize()];
                int n;
                while ((n = bz2.Read(buffer, 0, buffer.Length)) > 0) {
                    decompressed.Write(buffer, 0, n);
                }
            }

            Assert.IsTrue(decompressed.ToArray().SequenceEqual(PdfStream.ToArray()));
            Assert.Pass("Compressed size: {0} ({1:P} size reduction) Uncompressed size: {2}", compressed.Length, ((float) PdfStream.Length / (float) compressed.Length) - 1.0f, PdfStream.Length);
        }
    }
}
