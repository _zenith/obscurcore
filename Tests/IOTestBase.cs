﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace ObscurCore.Tests
{
    public abstract class IOTestBase
    {
        public const int DefaultBufferSize = 4096;

        public virtual int GetBufferSize () { return DefaultBufferSize; }

        private const int RandomStreamLength = 1024 * 1024; // 1 MB

        private readonly static string _pathTestRoot = Directory.GetParent(Environment.CurrentDirectory).FullName + Path.DirectorySeparatorChar;
		protected internal readonly static List<string> TestDataPaths = new List<string> ();
        protected readonly MemoryStream PdfStream = new MemoryStream ();
		protected readonly MemoryStream RandomStream = new MemoryStream();
        protected MemoryStream SmallStream;

		static IOTestBase ()
		{
            TestDataPaths.Add(_pathTestRoot + "testdata.pdf");
			TestDataPaths.Add (_pathTestRoot + "testdata1.pdf");
            TestDataPaths.Add(_pathTestRoot + "test.txt");
            TestDataPaths.Add(_pathTestRoot + "test1.txt");
		}

        [TestFixtureSetUp]
        public void InitFixture () {
            var fs = new FileStream(TestDataPaths[0], FileMode.Open, FileAccess.Read);
            fs.CopyTo(PdfStream);

            var rng = new Random();
            var data = new byte[2048];
            while (RandomStream.Length < RandomStreamLength) {
                rng.NextBytes(data);
                RandomStream.Write(data, 0, data.Length);
            }

            var rngBytes = new byte[32];
            rng.NextBytes(rngBytes);
            SmallStream = new MemoryStream(rngBytes);

            AuxTestFixtureInit();
        }

        [SetUp]
        public void InitTest () {
            PdfStream.Seek(0, SeekOrigin.Begin);
            RandomStream.Seek(0, SeekOrigin.Begin);
            AuxPerTestInit();
        }

        public virtual void AuxTestFixtureInit () { } // Implement as required by overriding
        public virtual void AuxPerTestInit () { } // Implement as required by overriding
    }
}
