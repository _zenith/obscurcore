using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

using ObscurCore;
using ObscurCore.Compression;
using ObscurCore.Cryptography;
using ObscurCore.Cryptography.PRNG;
using ObscurCore.DTO;
using ObscurCore.Packaging;
using ObscurCore.Knowledge;

using ObscurCore.Extensions.DTO;

using NUnit.Framework;
using ObscurCore.Cryptography.BouncyCastle.Security;

namespace ObscurCore.Tests.Packaging
{
	public class Payload
	{
	    private const string demuxDir = "demuxed";

        private readonly static List<FileInfo> _Files = new List<FileInfo>
	            {
	                //new FileInfo(IOTestBase.TestDataPaths[0]),
	                //new FileInfo(IOTestBase.TestDataPaths[1]),
	                new FileInfo(IOTestBase.TestDataPaths[2]),
                    new FileInfo(IOTestBase.TestDataPaths[3])
	            };

		public Payload ()
		{
		}

		[TestFixtureSetUp]
		public void InitFixture () {

		}

		[Test]
		public void Simple () {
			var items = GetItems (_Files.Count);
			var payloadConfig = PayloadLayoutConfigurationFactory.CreateDefault(PayloadLayoutSchemes.Simple);
			DoMux (payloadConfig, items, _Files);
		}

		[Test]
		public void Frameshift () {
			var items = GetItems (_Files.Count);
			var payloadConfig = PayloadLayoutConfigurationFactory.CreateDefault(PayloadLayoutSchemes.Frameshift);
			DoMux (payloadConfig, items, _Files);
		}

		[Test]
		public void Fabric () {
			var items = GetItems (_Files.Count);
            //var payloadConfig = PayloadLayoutConfigurationFactory.CreateDefault(PayloadLayoutSchemes.Fabric);

		    var payloadConfig = new PayloadLayoutConfiguration()
		        {
		            SchemeName = PayloadLayoutSchemes.Fabric.ToString(),
                    SchemeConfiguration = FabricConfigurationUtility.WriteFixedStriping(FabricMux.DefaultFixedStripeLength),
		            //SchemeConfiguration = FabricConfigurationUtility.WriteVariableStriping(FabricMux.MinimumStripeLength, FabricMux.MaximumStripeLength),
		            StreamPRNGName = "Salsa20",
		            StreamPRNGConfiguration = Salsa20GeneratorConfigurationUtility.WriteRandom(),
		            AuxillaryPRNGName = "Salsa20",
		            AuxillaryPRNGConfiguration = Salsa20GeneratorConfigurationUtility.WriteRandom()
		        };

			DoMux (payloadConfig, items, _Files);
		}

		protected List<PayloadItem> GetItems(int howMany) {
			var items = new List<PayloadItem> ();
            var sRng = StratCom.EntropySource;

		    for (int i = 0; i < howMany; i++) {
		        var payloadItem = new PayloadItem () {
				    Type = PayloadItemTypes.Binary,
				    //Compression = new CompressionConfiguration () { AlgorithmName = CompressionAlgorithms.LZ4.ToString() },
				    //Encryption = new BlockCipherConfiguration(SymmetricBlockCiphers.AES, BlockCipherModes.CTR, BlockCipherPaddingTypes.None),
                    Encryption = new StreamCipherConfiguration(SymmetricStreamCiphers.HC256)
			    };
                payloadItem.Encryption.Key = new byte[payloadItem.Encryption.KeySize / 8];
			    sRng.NextBytes(payloadItem.Encryption.Key);
			    
			    items.Add (payloadItem);
		    }

			return items;
		}

		protected List<Func<Stream, DecoratingStream>> GetTransforms(List<PayloadItem> items, bool writingPayload) {
            return items.Select(item => (Func<Stream, DecoratingStream>) (binding => item.BindTransformStream(writingPayload, binding, item.Encryption.Key))).ToList();
		}

	    protected void DoMux(PayloadLayoutConfiguration payloadConfig, List<PayloadItem> items, List<FileInfo> files,  bool outputPayload = false) {
			
            var ms = new MemoryStream ();
	        

	        

	        for (int index = 0; index < items.Count; index++) {
	            var payloadItem = items[index];
	            int index1 = index;
	            payloadItem.SetStreamBinding(() => new FileStream(files[index1].FullName, FileMode.Open));
	            payloadItem.ExternalLength = payloadItem.StreamBinding.Length;
	        }

	        var transforms = GetTransforms(items, true);
			var mux = Athena.Packaging.PayloadModules[payloadConfig.SchemeName].InstantiateImplementation
				(true, ms, items.ToList<IStreamBinding>(), transforms, payloadConfig);
			
			Assert.DoesNotThrow (mux.ExecuteAll);

            // Get internal lengths
	        for (int i = 0; i < items.Count; i++) {
	            items[i].InternalLength = mux.GetItemIO(i);
	        }

            var muxIn = mux.TotalSourceIO;

			foreach (var item in items) {
				item.StreamBinding.Close();
			}

            // Write out muxed payload - optional
	        if (outputPayload) {
                using (var fs = new FileStream(new FileInfo(IOTestBase.TestDataPaths[0]).Directory.FullName + 
                    Path.DirectorySeparatorChar + mux.ToString().ToLower() + ".payload", FileMode.Create)) {

                    ms.WriteTo(fs);
                }
	        }

            // DEMUX

	        for (int index = 0; index < items.Count; index++) {
	            var payloadItem = items[index];
	            int index1 = index;
	            payloadItem.SetStreamBinding(() => new FileStream(files[index1].Directory.FullName + Path.DirectorySeparatorChar +
	                demuxDir + Path.DirectorySeparatorChar + files[index1].Name, FileMode.Create));
	        }

	        transforms = GetTransforms(items, false);

	        ms.Seek(0, SeekOrigin.Begin);
            mux = Athena.Packaging.PayloadModules[payloadConfig.SchemeName].InstantiateImplementation
                (false, ms, items.ToList<IStreamBinding>(), transforms, payloadConfig);

            Assert.DoesNotThrow(mux.ExecuteAll);

	        var muxOut = mux.TotalDestinationIO;

            foreach (var item in items) {
                item.StreamBinding.Close();
            }

            if (mux is FrameshiftMux) {
                Assert.Pass("MuxOut: {0}, Overhead: {1:P4} ({2} bytes) ; MuxIn: {3}",
                    muxOut, ((double) mux.Overhead/(double) muxOut), mux.Overhead, muxIn);
            } else {
                Assert.Pass("MuxOut: {0}, MuxIn: {1}", muxOut, muxIn);
            }
	    }

	}
}

