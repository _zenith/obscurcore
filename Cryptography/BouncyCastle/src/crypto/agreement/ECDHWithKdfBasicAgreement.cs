using System;
using System.Collections;

using ObscurCore.Cryptography.BouncyCastle.Asn1;
using ObscurCore.Cryptography.BouncyCastle.Asn1.Nist;
using ObscurCore.Cryptography.BouncyCastle.Asn1.Pkcs;
using ObscurCore.Cryptography.BouncyCastle.Asn1.X9;
using ObscurCore.Cryptography.BouncyCastle.Crypto.Agreement.Kdf;
using ObscurCore.Cryptography.BouncyCastle.Crypto.Parameters;
using ObscurCore.Cryptography.BouncyCastle.Math;
using ObscurCore.Cryptography.BouncyCastle.Security;

namespace ObscurCore.Cryptography.BouncyCastle.Crypto.Agreement
{
	public class ECDHWithKdfBasicAgreement
		: ECDHBasicAgreement
	{
		private readonly string algorithm;
		private readonly IDerivationFunction kdf;

		public ECDHWithKdfBasicAgreement(
			string				algorithm,
			IDerivationFunction	kdf)
		{
			if (algorithm == null)
				throw new ArgumentNullException("algorithm");
			if (kdf == null)
				throw new ArgumentNullException("kdf");

			this.algorithm = algorithm;
			this.kdf = kdf;
		}

		public override BigInteger CalculateAgreement(
			ICipherParameters pubKey)
		{
			// Note that the ec.KeyAgreement class in JCE only uses kdf in one
			// of the engineGenerateSecret methods.

			BigInteger result = base.CalculateAgreement(pubKey);

			int keySize = GeneratorUtilities.GetDefaultKeySize(algorithm);

			DHKdfParameters dhKdfParams = new DHKdfParameters(
				new DerObjectIdentifier(algorithm),
				keySize,
				bigIntToBytes(result));

			kdf.Init(dhKdfParams);

			byte[] keyBytes = new byte[keySize / 8];
			kdf.GenerateBytes(keyBytes, 0, keyBytes.Length);

			return new BigInteger(1, keyBytes);
		}

		private byte[] bigIntToBytes(
			BigInteger r)
		{
			int byteLength = X9IntegerConverter.GetByteLength(privKey.Parameters.G.X);
			return X9IntegerConverter.IntegerToBytes(r, byteLength);
		}
	}
}
