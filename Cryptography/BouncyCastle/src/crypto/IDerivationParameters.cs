using System;

namespace ObscurCore.Cryptography.BouncyCastle.Crypto
{
    /**
     * Parameters for key/byte stream derivation classes
     */
    public interface IDerivationParameters
    {
    }
}
