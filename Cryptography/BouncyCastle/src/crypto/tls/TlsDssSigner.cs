using System;

using ObscurCore.Cryptography.BouncyCastle.Crypto.Parameters;
using ObscurCore.Cryptography.BouncyCastle.Crypto.Signers;

namespace ObscurCore.Cryptography.BouncyCastle.Crypto.Tls
{
	internal class TlsDssSigner
		: TlsDsaSigner
	{
		public override bool IsValidPublicKey(AsymmetricKeyParameter publicKey)
		{
			return publicKey is DsaPublicKeyParameters;
		}

	    protected override IDsa CreateDsaImpl()
	    {
			return new DsaSigner();
	    }
	}
}
