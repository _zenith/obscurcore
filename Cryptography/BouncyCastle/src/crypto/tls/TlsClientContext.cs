using System;

using ObscurCore.Cryptography.BouncyCastle.Security;

namespace ObscurCore.Cryptography.BouncyCastle.Crypto.Tls
{
	public interface TlsClientContext
	{
		SecureRandom SecureRandom { get; }

		SecurityParameters SecurityParameters { get; }

		object UserObject { get; set; }
	}
}
