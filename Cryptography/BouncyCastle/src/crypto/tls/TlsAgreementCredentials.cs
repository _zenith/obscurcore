using System;
using System.IO;

namespace ObscurCore.Cryptography.BouncyCastle.Crypto.Tls
{
	public interface TlsAgreementCredentials : TlsCredentials
	{
		/// <exception cref="IOException"></exception>
		byte[] GenerateAgreement(AsymmetricKeyParameter serverPublicKey);
	}
}
