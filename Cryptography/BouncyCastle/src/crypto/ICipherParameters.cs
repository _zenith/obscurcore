using System;

namespace ObscurCore.Cryptography.BouncyCastle.Crypto
{
    /**
     * all parameter classes implement this.
     */
    public interface ICipherParameters
    {
    }
}
