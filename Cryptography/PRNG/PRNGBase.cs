using System;

namespace ObscurCore.Cryptography.PRNG
{
	/// <summary>
	/// Base class to derive pseudorandom number generators (PRNGs) from.
	/// </summary>
	public abstract class PRNGBase : Random
	{
		public override int Next (int maxValue) {
		    var dbl = NextDouble();
            var num = Math.Abs(dbl) * maxValue;
		    num = Math.Round(num, MidpointRounding.AwayFromZero);
		    return (int) num;
		}

        public override int Next(int minValue, int maxValue)
        {
            var dbl = NextDouble();
            var num = (Math.Abs(dbl) * maxValue) + minValue;
		    num = Math.Round(num, MidpointRounding.AwayFromZero);
		    return (int) num;
        }
		
		public int NextInt() {
			/*var ary = new byte[4];
            NextBytes(ary);
            // The first bit has just as much a chance as being a 0 as it does a 1, with a CSPRNG source - right?
            return BitConverter.ToInt32(ary, 0);*/
			
			return (int) (NextDouble() * Int32.MaxValue); // TODO: Compare the output of this method with that of the UInt32 one!
		}
		
		public uint NextUInt32() {
			var ary = new byte[4];
			NextBytes(ary);
			// The first bit has just as much a chance as being a 0 as it does a 1, with a CSPRNG source - right? TODO: Better check the distribution!
			return BitConverter.ToUInt32(ary, 0);
		}

		public override double NextDouble () { return Sample(); }
		
		protected override double Sample () {
			var bytes = new byte[8];
			this.NextBytes(bytes);
			//var ul = BitConverter.ToUInt64(bytes, 0) / (1 << 11); // original
			var ul = BitConverter.ToUInt64(bytes, 0) >> 11; // cleaner version
			return ul / (double) (1UL << 53);
		}
	}
}

