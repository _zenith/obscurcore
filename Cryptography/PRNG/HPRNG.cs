﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ObscurCore.Cryptography.PRNG
{
    /// <summary>
    /// Hash-based Pseudorandom Number Generator (of my own devising)
    /// Simply sums a necessary number of bytes from the start of an iterated hash.
    /// </summary>
    /// <remarks>
    /// If a requested number range is between 32 and 2048, first calculate the required range: 2048 - 32 = 2016 = 7.906 bytes. 
    /// Use Ceiling function to return next integer up, e.g 8 in this case.
    /// Returned hash is for example 0C-1C-32-F1-A2-B9-00 . Number is thus 13 + 28 + 50 + 241 + 162 + 185 + 0 = 679
    /// </remarks>
    class HPRNG
    {
    }
}
