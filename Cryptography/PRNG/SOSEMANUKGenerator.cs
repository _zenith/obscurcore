using ObscurCore.Cryptography.Ciphers;
using ObscurCore.Cryptography.BouncyCastle.Crypto.Parameters;

namespace ObscurCore.Cryptography.PRNG
{
	/// <summary>
	/// Generates deterministic cryptographically secure pseudorandom number sequence 
	/// using internal Salsa20 stream cipher.
	/// </summary>
	public sealed class SOSEMANUKGenerator : PRNGBase
	{
		private readonly SOSEMANUKEngine _engine = new SOSEMANUKEngine();

		public SOSEMANUKGenerator (byte[] iv, byte[] key) {
			_engine.Init(true, new ParametersWithIV(new KeyParameter(key), iv));
		}

		public static SOSEMANUKGenerator CreateFromConfiguration(byte[] config) {
			byte[] iv, key;
			SOSEMANUKGeneratorConfigurationUtility.Read(config, out iv, out key);
			return new SOSEMANUKGenerator(iv, key);
		}
		
		public static SOSEMANUKGenerator CreateAndEmitConfiguration(out byte[] config) {
			config = SOSEMANUKGeneratorConfigurationUtility.WriteRandom();
			return CreateFromConfiguration(config);
		}

		public override void NextBytes (byte[] buffer) {
			_engine.GetRawKeystream (buffer, 0, buffer.Length);
		}
	}
}

