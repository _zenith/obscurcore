namespace ObscurCore.Cryptography
{
	public interface IMacWithSalt
	{
		void Init(byte[] key, byte[] salt);
	}
}

