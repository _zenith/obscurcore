using System;

namespace ObscurCore.Cryptography
{
	public class PaddingException : Exception
	{
		public PaddingException () : base("The ciphertext padding is corrupt.") { }
		public PaddingException (string message) : base("The ciphertext padding is corrupt.\n" + message) { }
	}
	
	public class AuthenticationException : Exception
	{
		public AuthenticationException (string message) : base("WARNING: Possible security issue!\n" + message) { }
	}
	
	/*public class DataLengthException : Exception
    {
        public DataLengthException(string message) : base(message) { }
    }*/
	
	public class IncompleteBlockException : Exception
	{
		public IncompleteBlockException () : base("The ciphertext data is not the expected length for the block size.") { }
		public IncompleteBlockException (string message) : base("The ciphertext data is not the expected length for the block size.\n" + message) { }
	}
}

