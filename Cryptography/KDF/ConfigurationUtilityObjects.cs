using System;
using System.IO;
using ObscurCore.Extensions.Generic;
using ObscurCore.Extensions.Streams;

namespace ObscurCore.Cryptography.KDF
{
	public static class ScryptConfigurationUtility
	{	
		public const byte DefaultIterationPower = 14, DefaultBlocks = 8, DefaultParallelisation = 2;
		
		/// <summary>
		/// Reads an scrypt configuration from shorthand byte array format, and outputs the 
		/// iterationPower, blocks and parallelisation parameters to external variable fields.
		/// </summary>
		/// <param name="config"></param>
		/// <param name='iterationPower'>
		/// Power to raise the iteration count by - i.e. 2^n iterations, where n is <paramref name="iterationPower"/>. 
		/// General-use cost increase. Use to scale cost/difficulty without changing CPU or memory cost directly, only time.
		/// </param>
		/// <param name='blocks'>
		/// Blocks to operate on. Increases memory cost, as this algorithm is memory-hard. 
		/// Use sparingly in constrained environment such as mobile. Scale according to memory advancements.
		/// </param>
		/// <param name='parallelisation'>
		/// How many co-dependant mix operations must be performed. 
		/// Can be run in parallel, hence the name. Increases CPU cost. Scale according to CPU speed advancements.
		/// </param>
		public static void Read(byte[] config, out byte iterationPower, out byte blocks, out byte parallelisation) {
		    using (var ms = new MemoryStream(config)) {
		        ms.ReadPrimitive(out iterationPower);
                ms.ReadPrimitive(out blocks);
                ms.ReadPrimitive(out parallelisation);
		    }
            if(!iterationPower.IsBetween<byte>(5, 20)) throw new ArgumentOutOfRangeException("iterationPower", "Power to raise the iteration count (iterations = n^power) of scrypt KDF is out of the range of 5 to 20.");
		}
		
		/// <summary>
		/// Writes an scrypt configuration in shorthand byte array format with the 
		/// specified iterationPower, blocks and parallelisation parameters.
		/// </summary>
		/// <param name='iterationPower'>
		/// Power to raise the iteration count by, e.g. 2^n iterations where n is 'iterationPower'. 
		/// General-use cost increase. Use to scale cost/difficulty without changing CPU or memory cost directly, only time.
		/// </param>
		/// <param name='blocks'>
		/// Blocks to operate on. Increases memory cost, as this algorithm is memory-hard. 
		/// Use sparingly in constrained environment such as mobile. Scale according to memory advancements.
		/// </param>
		/// <param name='parallelisation'>
		/// How many co-dependant mix operations must be performed. 
		/// Can be run in parallel, hence the name. Increases CPU cost. Scale according to CPU speed advancements.
		/// </param>
		/// <param name="output"></param>
		public static void Write(int iterationPower, int blocks, int parallelisation, out byte[] output) {
            // TODO: Check parameter validity.
            using (var ms = new MemoryStream()) {
                ms.WritePrimitive((byte)iterationPower);
                ms.WritePrimitive((byte)blocks);
                ms.WritePrimitive((byte)parallelisation);
                output = ms.ToArray();
            }
		}
	}

    // Not implementing PBKDF2 unless actually requested because it's inferior to scrypt (scrypt actually uses it!)
    // public static class PBKDF2ConfigurationUtility {}
}

