using System;
using System.Collections.Generic;
using System.IO;

using ObscurCore.Cryptography.BouncyCastle.Crypto;
using ObscurCore.Cryptography.BouncyCastle.Crypto.Agreement;
using ObscurCore.Cryptography.BouncyCastle.Crypto.Parameters;
using ObscurCore.Cryptography.BouncyCastle.Math;
using ObscurCore.Cryptography.BouncyCastle.Math.EC;
using ObscurCore.Knowledge;

namespace ObscurCore.Cryptography
{
	// Currently, only (ephemeral,static)-static exchanges are supported
	// In NIST terminology, C(1, 2) agreements are supported.
	// This means the sender has a static key and generates an ephemeral key as well, each exchange.
	// The reciepient only has to have a static key. This makes the scheme one-pass.
	
	// One-Pass Unified Model, C(1,2 EC-DHC). Defined in 6.2.1.2.
	
	/// <summary>
	/// 
	/// </summary>
	/// <remarks>
	/// Similar operation to SymmetricCryptoStream, but uses an optionally ephemeral symmetric key 
	/// created through the use of an in-built public-key (asymmetric) scheme - e.g. ECDH, RSA, NTRU.
	/// <para>
	/// When specifying the asymmetric keypair, these values can be ephemeral (generated through 
	/// a key-agreement performed "on the fly" for the purpose), which provides greater security 
	/// but lower convenience, or they can be static. When using an ephemeral keypair, optionally 
	/// it may be signed with the static keypair to provide non-repudiation. If not signed, this 
	/// will provide forward secrecy and deniability, but provides no authentication of the sender. 
	/// When using static keypairs, this is called - unsurprisingly - static-static Diffie-Hellman. 
	/// It's also possible to do ephemeral-static, where the recipient isn't required to be avaliable.
	/// </para>
	/// </remarks>
	public class UM1CryptoStream
	{
		private readonly IBasicAgreement     _agreement;
		private readonly IDerivationFunction _kdf;
		private readonly IMac                _mac;
		
		public static SymmetricCryptoStream Initialise(Stream stream, bool isEncrypting, bool leaveOpen) {
			
		}
		
	}
	
	
	
	
	
	
}

