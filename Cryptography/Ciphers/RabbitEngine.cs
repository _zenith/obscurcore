﻿using System;
using ObscurCore.Cryptography.BouncyCastle.Crypto;
using ObscurCore.Cryptography.BouncyCastle.Crypto.Parameters;
using ObscurCore.Cryptography.BouncyCastle.Crypto.Utilities;

namespace ObscurCore.Cryptography.Ciphers
{
    public sealed class RabbitEngine : IStreamCipher
    {
        // Stores engine state
        private byte[]          _workingKey     = null,
                                _workingIV      = null;

        private bool	        _initialised    = false;
        private readonly uint[] _x              = new uint[8],
                                _c              = new uint[8];
        private uint _carry;

        private readonly uint[] _constants = new uint[] { 0x4D34D34D, 0xD34D34D3, 0x34D34D34, 0x4D34D34D,
                                                          0xD34D34D3, 0x34D34D34, 0x4D34D34D, 0xD34D34D3 };

        public void Init (bool forEncryption, ICipherParameters parameters) {
            // forEncryption parameter is irrelevant for Rabbit as operations are symmetrical, 
            // but required by class interface

            var ivParams = parameters as ParametersWithIV;
            if (ivParams == null) throw new ArgumentException("Rabbit initialisation requires an IV.", "parameters");
            _workingIV = ivParams.GetIV();
            if (_workingIV == null || _workingIV.Length != 8)
                throw new ArgumentException("Rabbit requires exactly 8 bytes of IV.");

            var key = ivParams.Parameters as KeyParameter;
            if (key == null) throw new ArgumentException("Rabbit initialisation requires a key.", "parameters");
            _workingKey = key.GetKey();
            if (_workingKey.Length != 16) throw new ArgumentException("Rabbit requires exactly 16 bytes of key.", "parameters");

            Reset();
        }

        public string AlgorithmName {
            get { return "Rabbit"; }
        }

        public void Reset () {
            KeySetup(_workingKey);
            IVSetup(_workingIV);
            _initialised = true;
        }

        public byte ReturnByte (byte input) {
            //if (limitExceeded()) {
            //throw new MaxBytesExceededException("2^70 byte limit per IV; Change IV");
            //}
            if (!_initialised) throw new InvalidOperationException(AlgorithmName + " not initialised.");

            throw new NotImplementedException(); // TODO: yes... I need to buffer input, and I haven't gotten round to it yet.
            //return 0;
        }

        public void ProcessBytes (byte[] inBytes, int inOff, int len, byte[] outBytes, int outOff) {
            if (!_initialised) {
                throw new InvalidOperationException(AlgorithmName + " not initialised.");
            }

            if ((inOff + len) > inBytes.Length) {
                throw new DataLengthException("Input buffer too short.");
            }

            if ((outOff + len) > outBytes.Length) {
                throw new DataLengthException("Output buffer too short.");
            }

            //if (limitExceeded(len)) {
            //throw new MaxBytesExceededException("2^70 byte limit per IV would be exceeded; Change IV");
            //}
            
            XORKeystream(inBytes, inOff, len, outBytes, outOff);
        }

        #region Private implementation
        /// <summary>
        /// XORs generates keystream with input data.
        /// </summary>
        /// <param name="inBytes">Input byte array must be a multiple of 16 in length.</param>
        /// <returns></returns>
        private void XORKeystream (byte[] inBytes, int inOff, int len, byte[] outBytes, int outOff) {
            int truncatedLen;
            var blocks = Math.DivRem(len, 16, out truncatedLen);
            for (var i = 0; i < blocks; i++) {
                NextState();
                Pack.UInt32_To_LE(Pack.LE_To_UInt32(inBytes, inOff) ^ _x[0] ^ (_x[5] >> 16) ^ (_x[3] << 16), outBytes, outOff);
                Pack.UInt32_To_LE(Pack.LE_To_UInt32(inBytes, inOff + 4) ^ _x[2] ^ (_x[7] >> 16) ^ (_x[5] << 16), outBytes, outOff + 4);
                Pack.UInt32_To_LE(Pack.LE_To_UInt32(inBytes, inOff + 8) ^ _x[4] ^ (_x[1] >> 16) ^ (_x[7] << 16), outBytes, outOff + 8);
                Pack.UInt32_To_LE(Pack.LE_To_UInt32(inBytes, inOff + 12) ^ _x[6] ^ (_x[3] >> 16) ^ (_x[1] << 16), outBytes, outOff + 12);
                inOff += 16;
                outOff += 16;
            }
            if (truncatedLen == 0) return;

            var inTruncated = new byte[16];
            var outTruncated = new byte[16];
            Array.Copy(inBytes, inOff, inTruncated, 0, truncatedLen);
            NextState();
            Pack.UInt32_To_LE(Pack.LE_To_UInt32(inTruncated, 0) ^ _x[0] ^ (_x[5] >> 16) ^ (_x[3] << 16), outTruncated, 0);
            Pack.UInt32_To_LE(Pack.LE_To_UInt32(inTruncated, 4) ^ _x[2] ^ (_x[7] >> 16) ^ (_x[5] << 16), outTruncated, 4);
            Pack.UInt32_To_LE(Pack.LE_To_UInt32(inTruncated, 8) ^ _x[4] ^ (_x[1] >> 16) ^ (_x[7] << 16), outTruncated, 8);
            Pack.UInt32_To_LE(Pack.LE_To_UInt32(inTruncated, 12) ^ _x[6] ^ (_x[3] >> 16) ^ (_x[1] << 16), outTruncated, 12);
            Array.Copy(outTruncated, 0, outBytes, outOff, truncatedLen);
        }

        /// <summary>
        /// Get raw keystream in a multiple of 16 bytes length.
        /// </summary>
        private byte[] GenerateKeystream(int blocks) {
            var outBytes = new byte[16 * blocks];
            var outOffset = 0;
            for (var i = 0; i < blocks; i++)
            {
                NextState();
                Pack.UInt32_To_LE(_x[0] ^ (_x[5] >> 16) ^ (_x[3] << 16), outBytes, outOffset);
                Pack.UInt32_To_LE(_x[2] ^ (_x[7] >> 16) ^ (_x[5] << 16), outBytes, outOffset + 4);
                Pack.UInt32_To_LE(_x[4] ^ (_x[1] >> 16) ^ (_x[7] << 16), outBytes, outOffset + 8);
                Pack.UInt32_To_LE(_x[6] ^ (_x[3] >> 16) ^ (_x[1] << 16), outBytes, outOffset + 12);
                outOffset += 16;
            }
            return outBytes;
        }

        /// <summary>
        /// Initialise the engine state with key material.
        /// </summary>
        private void KeySetup (byte[] key) {
            var k = new uint[4];

            // Generate four subkeys
            k[0] = BitConverter.ToUInt32(key, 0);
            k[1] = BitConverter.ToUInt32(key, 4);
            k[2] = BitConverter.ToUInt32(key, 8);
            k[3] = BitConverter.ToUInt32(key, 12);

            // Generate initial state variables
            _x[0] = k[0];
            _x[2] = k[1];
            _x[4] = k[2];
            _x[6] = k[3];
            _x[1] = (k[3] << 16) | (k[2] >> 16);
            _x[3] = (k[0] << 16) | (k[3] >> 16);
            _x[5] = (k[1] << 16) | (k[0] >> 16);
            _x[7] = (k[2] << 16) | (k[1] >> 16);

            // Generate initial counter values
            _c[0] = RotLeft(k[2], 16);
            _c[2] = RotLeft(k[3], 16);
            _c[4] = RotLeft(k[0], 16);
            _c[6] = RotLeft(k[1], 16);
            _c[1] = (k[0] & 0xFFFF0000) | (k[1] & 0xFFFF);
            _c[3] = (k[1] & 0xFFFF0000) | (k[2] & 0xFFFF);
            _c[5] = (k[2] & 0xFFFF0000) | (k[3] & 0xFFFF);
            _c[7] = (k[3] & 0xFFFF0000) | (k[0] & 0xFFFF);

            // Clear carry bit
            _carry = 0;

            // Iterate the system four times
            for (var j = 0; j < 4; j++) NextState();

            // Iterate the counters
            for (var j = 0; j < 8; j++) _c[j] ^= _x[(j + 4) & 0x7];
        }

        /// <summary>
        /// Initialise the engine state with initialisation vector material.
        /// </summary>
        private void IVSetup (byte[] iv) {
            if (iv.Length != 8) throw new ArgumentException("IV must be 8 bytes in length.");
            var i = new uint[4];

            // Generate four subvectors
            i[0] = BitConverter.ToUInt32(iv, 0);
            i[2] = BitConverter.ToUInt32(iv, 4);
            i[1] = (i[0] << 16) | (i[2] & 0xFFFF0000);
            i[3] = (i[2] << 16) | (i[0] & 0x0000FFFF);

            // Modify counter values
            var subIndex = 0;
            for (var index = 0; index < 8; index++) {
                _c[index] ^= i[subIndex];
                if (++subIndex > 3) subIndex = 0;
            }

            // Iterate the system four times
            for (var j = 0; j < 4; j++) NextState();
        }

        private static uint RotLeft (uint x, int rot) {
            return (x << rot) | (x >> (32 - rot));
        }

        private void NextState () {
            // Temporary variables
            uint[] g = new uint[8], cOld = new uint[8];

            /* Save old counter values */
            for (var i = 0; i < 8; i++) cOld[i] = _c[i];

            /* Calculate new counter values */
            _c[0] += _constants[0] + _carry;
            for (var i = 1; i < 8; i++) {
                _c[i] += _constants[i] + Convert.ToUInt32(_c[i - 1] < cOld[i - 1]);
            }
            _carry = Convert.ToUInt32(_c[7] < cOld[7]);

            /* Calculate the g-functions */
            for (var i = 0; i < 8; i++) g[i] = GFunc(_x[i] + _c[i]);

            /* Calculate new state values */
            _x[0] = g[0] + RotLeft(g[7], 16) + RotLeft(g[6], 16);
            _x[1] = g[1] + RotLeft(g[0], 8) + g[7];
            _x[2] = g[2] + RotLeft(g[1], 16) + RotLeft(g[0], 16);
            _x[3] = g[3] + RotLeft(g[2], 8) + g[1];
            _x[4] = g[4] + RotLeft(g[3], 16) + RotLeft(g[2], 16);
            _x[5] = g[5] + RotLeft(g[4], 8) + g[3];
            _x[6] = g[6] + RotLeft(g[5], 16) + RotLeft(g[4], 16);
            _x[7] = g[7] + RotLeft(g[6], 8) + g[5];
        }

        /// <summary>
        /// Square a 32-bit unsigned integer to obtain the 64-bit result 
        /// and return the upper 32 bits XOR the lower 32 bits.
        /// </summary>
        private static uint GFunc (uint x) {
            // Construct high and low argument for squaring
            uint a = x & 0xFFFF;
            uint b = x >> 16;
            // Calculate high and low result of squaring
            uint h = ((((a * a) >> 17) + (a * b)) >> 15) + b * b;
            uint l = x * x;
            // Return high XOR low
            return h ^ l;
        }
        #endregion
    }
}