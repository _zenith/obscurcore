﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ObscurCore.Cryptography.BouncyCastle.Crypto;

namespace ObscurCore.Cryptography.Digests
{
    /// <summary>
    /// Base class to derive hash digest functions from. Provide buffer-block capabilities and common interface.
    /// </summary>
    public abstract class DigestBase : IDigest
    {
        protected readonly int _digestSize;

        public DigestBase(int digestSize) {
            _digestSize = digestSize;
        }

        /// <summary>
        /// Name of the algorithm implemented.
        /// </summary>
        public abstract string AlgorithmName { get; }

        /// <summary>
        /// Size in bytes of the hash produced.
        /// </summary>
        public int GetDigestSize () {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Size in bytes of the internal buffer.
        /// </summary>
        public int GetByteLength () {
            throw new NotImplementedException();
        }

        public abstract void Update (byte input);

        public abstract void BlockUpdate(byte[] input, int inOff, int length);

        public abstract int DoFinal(byte[] output, int outOff);

        public abstract void Reset();


        private int TransformBlock (byte[] inputBuffer, int inputOffset, int inputCount, byte[] outputBuffer, int outputOffset) {
            if (inputOffset < 0)
                throw new ArgumentOutOfRangeException("inputOffset", "offset out of range");
            if (outputOffset < 0)
                throw new ArgumentOutOfRangeException("outputOffset", "offset out of range");
            HashCore(inputBuffer, inputOffset, inputCount);
            return inputCount;
        }

    }
}
