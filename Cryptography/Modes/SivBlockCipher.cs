﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ObscurCore.Cryptography.BouncyCastle.Crypto;
using ObscurCore.Cryptography.BouncyCastle.Crypto.Macs;
using ObscurCore.Cryptography.BouncyCastle.Crypto.Modes;

namespace ObscurCore.Cryptography.Modes
{
    class SivBlockCipher : IAeadBlockCipher
    {
        private SicBlockCipher _cipher; // CTR/SIC mode cipher
        private CMac _cmac; // CMAC/OMAC1  
        private bool _forEncryption;
        private int _blockSize;

        public static readonly int[] AllowedKeySizes = new[] { 32, 48, 64 }; // In bits: 256, 384, 512

        public SivBlockCipher (IBlockCipher cipher) {
            

            // Derive SIV

            // Initialise the CTR mode with derivative of v : 31st and 63rd bits zeroed
        }

        public string AlgorithmName {
            get { throw new NotImplementedException(); }
        }

        public void Init (bool forEncryption, ICipherParameters parameters) {
            throw new NotImplementedException();
        }

        public int GetBlockSize () {
            throw new NotImplementedException();
        }

        public int ProcessByte (byte input, byte[] outBytes, int outOff) {
            throw new NotImplementedException();
        }

        public int ProcessBytes (byte[] inBytes, int inOff, int len, byte[] outBytes, int outOff) {
            throw new NotImplementedException();
        }

        public int DoFinal (byte[] outBytes, int outOff) {
            throw new NotImplementedException();
        }

        public byte[] GetMac () {
            throw new NotImplementedException();
        }

        public int GetUpdateOutputSize (int len) {
            throw new NotImplementedException();
        }

        public int GetOutputSize (int len) {
            throw new NotImplementedException();
        }

        public void Reset () {
            throw new NotImplementedException();
        }
    }
}
