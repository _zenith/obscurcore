﻿using System;
using System.Collections;
using System.Collections.Generic;

using ObscurCore.Cryptography.BouncyCastle.Crypto;
using ObscurCore.Cryptography.BouncyCastle.Crypto.Modes;
using ObscurCore.Cryptography.BouncyCastle.Crypto.Parameters;

namespace ObscurCore.Cryptography
{
    // Put in preprocessor directive to allow exclusion of this due to patent-protection in the U.S.

    /// <summary>
    /// Implements 'Offset CodeBook' AEAD scheme. High performance, simple, and only patent-protected in the United States and for non-GPL code.
    /// </summary>
    public sealed class OcbBlockCipher : IAeadBlockCipher
    {
        // Internal cipher. Anything that uses 128 bit blocks.
        private IBlockCipher _cipher; // Cipher must be 128-bit block cipher. Key size is unrestricted.
        // State variables
        private readonly byte[] _L_star, _L_dollar; // L*, L$
        private readonly byte[][] _L_i = new byte[32][]; // stores L_0 ... L_n for HASH, ENCRYPT, and DECRYPT functions.
        private int _bottom;
        private byte[] _Ktop, _Stretch = new byte[24], _Offset = new byte[16], _Checksum = new byte[16];
        private int _i = 1;

        // Nonce should be less than 128 bits (16 bytes) in length. AD can be any length. _macBlock stores most recent hash operation result.
        private byte[] _macBlock = new byte[16], _nonce = new byte[16], _associatedText, _bufferBlock;
        private int _bufferPosition;
        private readonly Queue<byte[]> _blockQueue = new Queue<byte[]>();
        private bool _forEncryption;

        public const int TAGLEN = 128; // Number of bits in the tag (bytes stored is this / 8). MUST BE A MULTIPLE OF 8 !!!

        // Constructor. We precalculate the values for L_x family variables here before starting anything else.
        public OcbBlockCipher(IBlockCipher cipher) {
            // Is the requested cipher block size compatible?
            if(cipher.GetBlockSize() != 16) throw new ArgumentException("OCB mode can only use ciphers with a 128 bit block size.");
            this._cipher = cipher;
            // Precalculate L*, L$, L_i [0-32]
            _L_star = new byte[16];
            _cipher.ProcessBlock(new byte[16], 0, _L_star, 0); // initialise L*
            _L_dollar = doubleS(_L_star);
            _L_i[0] = (doubleS(_L_dollar)); // L_0 init for subsequent loop-gen
            for (int i = 1; i < 32; i++) {
                // Precalculate L_i for all trailing-zero values possible for a 32 bit integer. Zero-based, so 31 elements.
                // Insert it into the array of arrays
                _L_i[i] = doubleS(_L_i[i - 1]);
            }
        }

        public string AlgorithmName {
            get { return _cipher.AlgorithmName + "/OCB"; }
        }

        public void Init(bool forEncryption, ICipherParameters parameters) {
            this._forEncryption = forEncryption;
            ICipherParameters keyParam;
            byte[] suppliedNonce;
            if (parameters is AeadParameters) {
                AeadParameters param = (AeadParameters) parameters;
                suppliedNonce = param.GetNonce();
                _associatedText = param.GetAssociatedText();
                keyParam = param.Key;
            } else if (parameters is ParametersWithIV) {
                ParametersWithIV param = (ParametersWithIV) parameters;
                suppliedNonce = param.GetIV();
                _associatedText = new byte[0];
                keyParam = param.Parameters;
            } else {
                throw new ArgumentException("invalid parameters passed to OCB");
            }
            _bufferBlock = _forEncryption ? new byte[16] : new byte[16 + (TAGLEN/8)]; // TODO: Possible solution for the ensuring tag read when decrypting problem.
            // Now init working nonce, bottom, Ktop, etc.
            if(suppliedNonce.Length > 15) throw new ArgumentException("Supplied nonce is too large. OCB requires a nonce of 112 bits or less, or 96 preferably.");
            Array.Copy(suppliedNonce, 0, _nonce, _nonce.Length - suppliedNonce.Length, suppliedNonce.Length);
            _nonce[_nonce.Length - suppliedNonce.Length - 1] = 0x01; // Set the bit following the trailing zeros to 1 (prepends the supplied nonce).
            BitArray bottomBitArray = new BitArray(new [] {_nonce[15]});
            // Zero out bits 1-3 of 8
            bottomBitArray[0] = false;
            bottomBitArray[1] = false;
            bottomBitArray[2] = false;
            // Cast byte to an int to get the number out
            int[] result = new int[1];
            bottomBitArray.CopyTo(result, 0);
            // Assign result to bottom
            _bottom = result[0];
            // Before constructing Ktop we need to initialise the internal cipher
            _cipher.Init(forEncryption, keyParam);
            // Construct Ktop
            BitArray ktopBitArray = new BitArray(_nonce);
            // Zero out last 6 bits
            ktopBitArray[122] = false;
            ktopBitArray[123] = false;
            ktopBitArray[124] = false;
            ktopBitArray[125] = false;
            ktopBitArray[126] = false;
            ktopBitArray[127] = false;
            byte[] ktopIntermediate = new byte[16],  ktopCipherOut = new byte[16];
            ktopBitArray.CopyTo(ktopIntermediate, 0);
            // Process Ktop with cipher
            _cipher.ProcessBlock(ktopIntermediate, 0, ktopCipherOut, 0);
            _Ktop = ktopCipherOut;
            ktopBitArray = new BitArray(_Ktop);
            // Construct Stretch
            bool[] stretchBoolArray = new bool[192]; // 24 byte byte-array-to-be
            ktopBitArray.CopyTo(stretchBoolArray, 0);
            for (int i = 0; i < 64; i++) {
                stretchBoolArray[i + 128] = ktopBitArray[i] ^ ktopBitArray[i + 8];
            }
            BitArray stretchBitArray = new BitArray(stretchBoolArray);
            stretchBitArray.CopyTo(_Stretch, 0);
            // Construct Offset
            bool[] offsetBoolArray = new bool[128];
            // Offset = 128 bits from Stretch, offset by Bottom. We can just copy them in.
            Array.Copy(stretchBoolArray, _bottom, offsetBoolArray, 0, 128);
            BitArray offsetBitArray = new BitArray(offsetBoolArray);
            offsetBitArray.CopyTo(_Offset, 0);
            // Checksum has already been initialised, so we're all done here!
        }

        public int GetBlockSize() {
            return 16; // bytes. 128 bits.
        }

        public int ProcessByte(byte input, byte[] outBytes, int outOff) {
            // Can we write into the temp buffer?
            if (_bufferPosition < 16) _bufferBlock[_bufferPosition++] = input;
            if (_bufferPosition == 16) {
                _blockQueue.Enqueue(_bufferBlock);
                _bufferPosition = 0;
                // Process block here, output it through outBytes
                return 16;
            }
            // Nothing to return yet, not a full block to process
            return 0;
        }

        public int ProcessBytes(byte[] inBytes, int inOff, int len, byte[] outBytes, int outOff) {
            // Check parameter validity
            if (inOff + len >= inBytes.Length)
                throw new ArgumentOutOfRangeException("len or inOff", "There is insufficient data in the source array for the specified offset and length.");
            // Start processing - construct and enqueue the current batch of blocks supplied in inBytes
            int bytesEnqueued = EnqueueBlocks(inBytes, inOff, len);
            // If no full blocks to process, just exit function now
            if (bytesEnqueued == 0) return 0;
            // Process bytes blockwise here with encrypt or decrypt (depends on state), and save results in outBytes (passed by ref in parameters)
            return _forEncryption ? Encrypt(outBytes, outOff, false) : Decrypt(outBytes, outOff, false);
        }

        private int EnqueueBlocks(byte[] inBytes, int inOff, int len) {
            int remainder;
            int blocks = Math.DivRem(len, 16, out remainder);
            // Enqueue all the full blocks for processing
            byte[] block = new byte[16];
            for (int i = 0; i < blocks; i++) {
                Array.Copy(inBytes, inOff + (i * 16), block, 0, 16);
                _blockQueue.Enqueue(block);
            }
            // If there's any partial block data to process, do this bit...
            if (remainder > 0) {
                block = new byte[remainder];
                Array.Copy(inBytes, inOff + (16 * blocks), block, 0, remainder);
                // Process the remaining partial block
                if (_bufferPosition + remainder <= 16) {
                    Array.Copy(block, 0, _bufferBlock, _bufferPosition, remainder);
                    _bufferPosition += remainder;
                } else {
                    int bufferRequirement = 16 - _bufferPosition;
                    Array.Copy(block, 0, _bufferBlock, _bufferPosition, bufferRequirement);
                    remainder -= bufferRequirement;
                    _blockQueue.Enqueue(_bufferBlock);
                    Array.Copy(block, 0, _bufferBlock, 0, remainder);
                    _bufferPosition = remainder;
                }
            }
            if (_blockQueue.Count == 0) return 0; // Skip a mul op
            return _blockQueue.Count * 16;
        }

        public int DoFinal (byte[] outBytes, int outOff) { return Encrypt(outBytes, outOff, true); }

        private int Encrypt (byte[] outBytes, int outOff, bool final) {
            int bytesWritten;
            if (final) {
                byte[] tag = new byte[TAGLEN / 8];
                bytesWritten = Crypt(outBytes, outOff, tag, true);
                Array.Copy(tag, 0, outBytes, outOff + bytesWritten, tag.Length);
                bytesWritten += tag.Length;
            } else {
                bytesWritten = Crypt(outBytes, outOff, new byte[0], false);
            }
            return bytesWritten;
        }

        private int Decrypt (byte[] outBytes, int outOff, bool final) {
            int bytesRead;
            if (final) {
                byte[] expectedTag = new byte[TAGLEN / 8];
                bytesRead = Crypt(outBytes, outOff, expectedTag, true);
                // Check if the produced tag matches the one in the ciphertext||tag data.
                // Make some changes to the Crypt function so that it outputs the tag when acting as decryptor, rather than trying to decrypt the tag
                // This could be done by processing all but one of the full blocks in Crypt when 'final' is set to true?
            } else {
                bytesRead = Crypt(outBytes, outOff, new byte[0], false);
            }
            return bytesRead;
        }

        private int Crypt(byte[] outBytes, int outOff, byte[] tag, bool final) {
            byte[] currentBlock, currentBlockOutput = new byte[16];
            int bytesProcessed = 0;
            while (!_forEncryption ? _blockQueue.Count > 1 : _blockQueue.Count > 0) { // TODO: Leave a block if decrypting. Add relevant functionality further down kthx.
                // Could put this block in a seperate function? Could be useful for tag processing when in decrypt & final state.
                XORByteArrayInPlace(_Offset, _L_i[CalculateNTZ(_i)]);
                currentBlock = _blockQueue.Dequeue();
                _cipher.ProcessBlock(currentBlock, 0, currentBlockOutput, 0);
                XORByteArrayInPlace(currentBlockOutput, _Offset);
                // Output the processed block by copying its bytes into outBytes
                Array.Copy(currentBlockOutput, 0, outBytes, outOff + bytesProcessed, 16);
                XORByteArrayInPlace(_Checksum, currentBlock);
                // Increment the offset to reflect the block write
                bytesProcessed += 16;
                _i++; // Increment the block counter
            }
            // Process any final partial block and compute raw tag
            if (final) {
                if (_forEncryption) { // Easier to just process it this way, with two inner mini-functions (encryption/decryption)
                    tag = new byte[TAGLEN / 8];
                    if (_bufferPosition > 0) {
                        byte[] offsetStar = XORByteArray(_Offset, _L_star);
                        byte[] pad = new byte[16];
                        _cipher.ProcessBlock(offsetStar, 0, pad, 0);
                        byte[] cStar = new byte[_bufferPosition + 1];
                        Array.Copy(_bufferBlock, 0, cStar, 0, _bufferPosition);
                        XORByteArrayInPlace(cStar, pad);
                        // Output the processed partial block by copying its bytes into outBytes
                        Array.Copy(cStar, 0, outBytes, outOff + bytesProcessed, _bufferPosition);
                        // Increment the offset to reflect the partial block write
                        bytesProcessed += _bufferPosition;
                        byte[] checksumStar = new byte[16];
                        Array.Copy(_bufferBlock, 0, checksumStar, 0, _bufferPosition);
                        checksumStar[_bufferPosition] = 0x80;
                        XORByteArrayInPlace(checksumStar, _Checksum);
                        byte[] tagInput = XORByteArray(checksumStar, XORByteArray(offsetStar, _L_star));
                        _cipher.ProcessBlock(tagInput, 0, tag, 0);
                    } else {
                        byte[] tagInput = XORByteArray(_Checksum, XORByteArray(_Offset, _L_star));
                        _cipher.ProcessBlock(tagInput, 0, tag, 0);
                    }
                    // XOR the tag thus far produced with output of HASH
                    XORByteArrayInPlace(tag, Hash(_associatedText));
                } else {
                    // Build a final (probably not not always partial - it's a 15/16 chance, 93.75% probability) block to prepare for partitioning off TAG portion
                    // It will [probably] consist of part of a block, and the tempbuf contents ...
                    // ... unless the plaintext length was a multiple of block size *and* TAGLEN = block size.

                    // Also started implementing a solution for doing it by extending the size of the tempbuf when in decrypt mode. See Init function.
                    byte[] block_star;
                    // TODO: Implement the rest of me!
                }
            }
            return bytesProcessed;
        }

        private byte[] Hash (byte[] A) {
            if(A.Length == 0) return new byte[16]; // If associated data is empty, return 128 bits of 0 (zeros(128)). Fast.
            // Consider A as a sequence of 128-bit blocks
            int A_star_length;
            int fullBlocks = Math.DivRem(A.Length, 16, out A_star_length);
            // Process any full/whole blocks
            byte[] sum = new byte[16];
            byte[] offset = new byte[16];
            byte[] adBlock = new byte[16], cipherOut = new byte[16];
            for (int i = 0; i < fullBlocks; i++) {
                // XOR all bytes of offset with itself and L_{ntz[i]}
                XORByteArrayInPlace(offset, _L_i[CalculateNTZ(i)]);
                // Now grab a block of associated data and put it in our AD tempbuf 'adBlock'
                Array.Copy(A, i * 16, adBlock, 0, 16);
                // XOR the AD buffer data with offset and process the result with the cipher
                _cipher.ProcessBlock(XORByteArray(adBlock, offset), 0, cipherOut, 0);
                // XOR the sum with the cipher output
                XORByteArrayInPlace(sum, cipherOut);
            }
            // Process any final partial block; compute final hash value
            if (A_star_length > 0) {
                byte[] offset_star = XORByteArray(offset, _L_star);
                byte[] cipherInput = new byte[16];
                // Copy in the A_star block fragment
                Array.Copy(A, A.Length - A_star_length, cipherInput, 0, A_star_length);
                // Set the HSB (10000000) in the first byte of the set of padding zeros.
                cipherInput[(A.Length - A_star_length) + 1] = 0x80; // [10000000] byte
                // XOR the [A_* with appended set bit in post-trailing zeros] with offset_*
                XORByteArrayInPlace(cipherInput, offset_star);
                // Process the cipher input with the cipher
                _cipher.ProcessBlock(cipherInput, 0, cipherOut, 0);
                // XOR the accumulated sum with the cipher output
                XORByteArrayInPlace(sum, cipherOut);
            }
            // Return the generated hash byte array (16 bytes / 128 bits)
            return sum;
        }

        public byte[] GetMac () {
            throw new NotImplementedException();
        }

        public int GetUpdateOutputSize (int len) {
            throw new NotImplementedException();
        }

        public int GetOutputSize (int len) {
            throw new NotImplementedException();
        }

        public void Reset () {
            throw new NotImplementedException();
        }

        #region Byte array XOR methods
        internal static byte[] XORByteArray(byte[] A, byte[] B) {
            byte[] outArray = new byte[A.Length];
            for (int i = 0; i < A.Length; i++) {
                outArray[i] = (byte)(A[i]^B[i]);
            }
            return outArray;
        }

        internal static void XORByteArrayInPlace (byte[] target, byte[] src) {
            for (int i = 0; i < src.Length; i++) {
                target[i] ^= src[i];
            }
        }

        internal static void XORByteArrayInPlace (byte[] target, int destIndex, int length, byte[] src) {
            int endIndex = destIndex + length;
            for (int i = destIndex; i < endIndex; i++) {
                target[i] ^= src[i];
            }
        }
        #endregion

        #region Non-trailing zero related
        internal static int CalculateNTZ (int i) {
            return NtzLookup[(i & -i) % 37];
        }
        private static readonly int[] NtzLookup = {
            32, 0, 1, 26, 2, 23, 27, 0, 3, 16, 24, 30, 28, 11, 0, 13, 4, 7, 17,
            0, 25, 22, 31, 15, 29, 10, 12, 6, 0, 21, 14, 9, 5, 20, 8, 19, 18
        };
        #endregion

        // Y U BE SO ANNOYING TO IMPLEMENT!? Probably slow after all this too. Might be worth doing an unsafe function with fast and diiirty bitwise ops.
        private byte[] doubleS(byte[] S) {
            // Check to see if passed byte array is 16 bytes long? Or will this hurt performance too much?
            BitArray bitArrayOrig = new BitArray(S), bitArrayNew = new BitArray(128);
            byte[] outArray = new byte[16];
            for (int i = 1; i < 128; i++) {
                bitArrayNew[i - 1] = bitArrayOrig[i];
            }
            bitArrayNew[127] = false;
            if (bitArrayOrig[0] == false) {
                bitArrayNew.CopyTo(outArray, 0);
                return outArray;
            }
            // Not the first plain case if we got to here, let's do the XOR case.
            BitArray bitArrayXor = new BitArray(128);
            bool[] postpendBits = new [] {true, false, false, false, false, true, true, true};
            for (int i = 127; i >= 120; i--) {
                bitArrayXor[i] = postpendBits[127 - i];
            }
            bitArrayNew.Xor(bitArrayXor).CopyTo(outArray, 0);
            return outArray;
        }
    }
}
