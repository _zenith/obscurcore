using System.Collections.Generic;

using ObscurCore.Cryptography;
using ObscurCore.Packaging;
using ObscurCore.Knowledge;

namespace ObscurCore
{
    /// <summary>
    /// Athena provides knowledge of how all core functions must be configured for proper operation 
	/// and provides low friction instantiation facilities for core objects.
    /// </summary>
    public static class Athena
    {
        static Athena() {
            
        }

        public static class Cryptography
        {
            static Cryptography() {
                // Add all symmetric block ciphers
                SymmetricCiphers.Add(SymmetricBlockCiphers.AES.ToString(), new SymmetricCipherDescription {
                    Name = SymmetricBlockCiphers.AES.ToString(),
                    DisplayName = "Advanced Encryption Standard (AES)",
                    AllowableBlockSizes = new[] { 128 },
                    DefaultBlockSize = 128,
                    AllowableKeySizes = new[] { 128, 192, 256 },
                    DefaultKeySize = 256
                });
                SymmetricCiphers.Add(SymmetricBlockCiphers.Blowfish.ToString(), new SymmetricCipherDescription {
                    Name = SymmetricBlockCiphers.Blowfish.ToString(),
                    DisplayName = "Blowfish",
                    AllowableBlockSizes = new[] { 64 },
                    DefaultBlockSize = 64,
                    AllowableKeySizes = new[] { 32, 64, 96, 128, 160, 192, 224, 256, 288, 320, 352, 384, 416, 448 },
                    DefaultKeySize = 256
                });
                SymmetricCiphers.Add(SymmetricBlockCiphers.Camellia.ToString(), new SymmetricCipherDescription {
                    Name = SymmetricBlockCiphers.Camellia.ToString(),
                    DisplayName = "Camellia",
                    AllowableBlockSizes = new[] { 128 },
                    DefaultBlockSize = 128,
                    AllowableKeySizes = new[] { 128, 192, 256 },
                    DefaultKeySize = 256
                });
                SymmetricCiphers.Add(SymmetricBlockCiphers.CAST5.ToString(), new SymmetricCipherDescription {
                    Name = SymmetricBlockCiphers.CAST5.ToString(),
                    DisplayName = "CAST-5 / CAST-128",
                    AllowableBlockSizes = new[] { 64 },
                    DefaultBlockSize = 64,
                    AllowableKeySizes = new[] { 40, 48, 56, 64, 72, 80, 88, 96, 104, 112, 120, 128 },
                    DefaultKeySize = 128
                });
                SymmetricCiphers.Add(SymmetricBlockCiphers.CAST6.ToString(), new SymmetricCipherDescription {
                    Name = SymmetricBlockCiphers.CAST6.ToString(),
                    DisplayName = "CAST-6 / CAST-256",
                    AllowableBlockSizes = new[] { 128 },
                    DefaultBlockSize = 128,
                    AllowableKeySizes = new[] { 128, 160, 192, 224, 256 },
                    DefaultKeySize = 256
                });
                SymmetricCiphers.Add(SymmetricBlockCiphers.GOST28147.ToString(), new SymmetricCipherDescription {
                    Name = SymmetricBlockCiphers.GOST28147.ToString(),
                    DisplayName = "GOST 28147-89",
                    AllowableBlockSizes = new[] { 64 },
                    DefaultBlockSize = 64,
                    AllowableKeySizes = new[] { 256 },
                    DefaultKeySize = 256
                });
                SymmetricCiphers.Add(SymmetricBlockCiphers.IDEA.ToString(), new SymmetricCipherDescription {
                    Name = SymmetricBlockCiphers.IDEA.ToString(),
                    DisplayName = "IDEA",
                    AllowableBlockSizes = new[] { 64 },
                    DefaultBlockSize = 64,
                    AllowableKeySizes = new[] { 128 },
                    DefaultKeySize = 128
                });
                SymmetricCiphers.Add(SymmetricBlockCiphers.NOEKEON.ToString(), new SymmetricCipherDescription {
                    Name = SymmetricBlockCiphers.NOEKEON.ToString(),
                    DisplayName = "NOEKEON",
                    AllowableBlockSizes = new[] { 128 },
                    DefaultBlockSize = 128,
                    AllowableKeySizes = new[] { 128 },
                    DefaultKeySize = 128
                });
                SymmetricCiphers.Add(SymmetricBlockCiphers.RC6.ToString(), new SymmetricCipherDescription {
                    Name = SymmetricBlockCiphers.RC6.ToString(),
                    DisplayName = "RC6",
                    AllowableBlockSizes = new[] { 128 },
                    DefaultBlockSize = 128,
                    AllowableKeySizes = new[] { 128, 192, 256 },
                    DefaultKeySize = 256
                });
                SymmetricCiphers.Add(SymmetricBlockCiphers.Rijndael.ToString(), new SymmetricCipherDescription {
                    Name = SymmetricBlockCiphers.Rijndael.ToString(),
                    DisplayName = "Rijndael",
                    AllowableBlockSizes = new[] { 128, 192, 256 },
                    DefaultBlockSize = 128,
                    AllowableKeySizes = new[] { 128, 192, 256 },
                    DefaultKeySize = 256
                });
                SymmetricCiphers.Add(SymmetricBlockCiphers.Serpent.ToString(), new SymmetricCipherDescription {
                    Name = SymmetricBlockCiphers.Serpent.ToString(),
                    DisplayName = "Serpent",
                    AllowableBlockSizes = new[] { 128 },
                    DefaultBlockSize = 128,
                    AllowableKeySizes = new[] { 128, 192, 256 },
                    DefaultKeySize = 256
                });
                SymmetricCiphers.Add(SymmetricBlockCiphers.TripleDES.ToString(), new SymmetricCipherDescription {
                    Name = SymmetricBlockCiphers.TripleDES.ToString(),
                    DisplayName = "Triple DES / 3DES / DESEDE",
                    AllowableBlockSizes = new[] { 64 },
                    DefaultBlockSize = 64,
                    AllowableKeySizes = new[] { 128, 192 },
                    DefaultKeySize = 192
                });
                SymmetricCiphers.Add(SymmetricBlockCiphers.Twofish.ToString(), new SymmetricCipherDescription {
                    Name = SymmetricBlockCiphers.Twofish.ToString(),
                    DisplayName = "Twofish",
                    AllowableBlockSizes = new[] { 128 },
                    DefaultBlockSize = 128,
                    AllowableKeySizes = new[] { 128, 192, 256 },
                    DefaultKeySize = 256
                });
                // Add all symmetric stream ciphers
                SymmetricCiphers.Add(SymmetricStreamCiphers.HC128.ToString(), new SymmetricCipherDescription {
                    Name = SymmetricStreamCiphers.HC128.ToString(),
                    DisplayName = "HC-128",
                    AllowableBlockSizes = new[] { -1 },
                    DefaultBlockSize = -1,
                    AllowableIVSizes = new[] { 128 },
                    DefaultIVSize = 128,
                    AllowableKeySizes = new[] { 128 },
                    DefaultKeySize = 128
                });
                SymmetricCiphers.Add(SymmetricStreamCiphers.HC256.ToString(), new SymmetricCipherDescription {
                    Name = SymmetricStreamCiphers.HC256.ToString(),
                    DisplayName = "HC-256",
                    AllowableBlockSizes = new[] { -1 },
                    DefaultBlockSize = -1,
                    AllowableIVSizes = new[] { 256 },
                    DefaultIVSize = 256,
                    AllowableKeySizes = new[] { 256 },
                    DefaultKeySize = 256
                });
                SymmetricCiphers.Add(SymmetricStreamCiphers.Rabbit.ToString(), new SymmetricCipherDescription {
                    Name = SymmetricStreamCiphers.Rabbit.ToString(),
                    DisplayName = "Rabbit",
                    AllowableBlockSizes = new[] { -1 },
                    DefaultBlockSize = -1,
                    AllowableIVSizes = new[] { 64 },
                    DefaultIVSize = 64,
                    AllowableKeySizes = new[] { 128 },
                    DefaultKeySize = 128
                });
                SymmetricCiphers.Add(SymmetricStreamCiphers.RC4.ToString(), new SymmetricCipherDescription {
                    Name = SymmetricStreamCiphers.RC4.ToString(),
                    DisplayName = "RC4",
                    AllowableBlockSizes = new[] { -1 },
                    DefaultBlockSize = -1,
                    AllowableIVSizes = new[] { 40, 56, 96, 128, 192, 256 },
                    DefaultIVSize = 128,
                    AllowableKeySizes = new[] { 40, 56, 96, 128, 192, 256 },
                    DefaultKeySize = 128
                });
                SymmetricCiphers.Add(SymmetricStreamCiphers.Salsa20.ToString(), new SymmetricCipherDescription {
                    Name = SymmetricStreamCiphers.Salsa20.ToString(),
                    DisplayName = "Salsa20",
                    AllowableBlockSizes = new[] { -1 },
                    DefaultBlockSize = -1,
                    AllowableIVSizes = new[] { 64 },
                    DefaultIVSize = 64,
                    AllowableKeySizes = new[] { 256 },
                    DefaultKeySize = 256
                });
                SymmetricCiphers.Add(SymmetricStreamCiphers.SOSEMANUK.ToString(), new SymmetricCipherDescription {
                    Name = SymmetricStreamCiphers.SOSEMANUK.ToString(),
                    DisplayName = "SOSEMANUK",
                    AllowableBlockSizes = new[] { -1 },
                    DefaultBlockSize = -1,
                    AllowableIVSizes = new[] { 128 },
                    DefaultIVSize = 128,
                    AllowableKeySizes = new[] { 256 },
                    DefaultKeySize = 256
                });
                SymmetricCiphers.Add(SymmetricStreamCiphers.VMPC.ToString(), new SymmetricCipherDescription {
                    Name = SymmetricStreamCiphers.VMPC.ToString(),
                    DisplayName = "Variably Modified Permutation Composition (VMPC)",
                    AllowableBlockSizes = new[] { -1 },
                    DefaultBlockSize = -1,
                    AllowableIVSizes = new[] { 128, 192, 256 },
                    DefaultIVSize = 256,
                    AllowableKeySizes = new[] { 128, 192, 256 },
                    DefaultKeySize = 256
                });

                // Add block cipher modes of operation
                SymmetricCipherModes.Add(BlockCipherModes.CBC.ToString(), new SymmetricCipherModeDescription {
                    Name = BlockCipherModes.CBC.ToString(),
                    DisplayName = "Ciphertext Block Chaining (CBC)",
                    PaddingRequirement = PaddingRequirements.Always,
                    AllowableBlockSizes = new[] { -1 },
                    IsAEADMode = false,
                    NonceReusePolicy = NonceReusePolicies.NotApplicable
                });
                SymmetricCipherModes.Add(BlockCipherModes.CFB.ToString(), new SymmetricCipherModeDescription {
                    Name = BlockCipherModes.CFB.ToString(),
                    DisplayName = "Cipher Feedback (CFB)",
                    PaddingRequirement = PaddingRequirements.None,
                    AllowableBlockSizes = new[] { -1 },
                    IsAEADMode = false,
                    NonceReusePolicy = NonceReusePolicies.NotApplicable
                });
                SymmetricCipherModes.Add(BlockCipherModes.CTR.ToString(), new SymmetricCipherModeDescription {
                    Name = BlockCipherModes.CTR.ToString(),
                    DisplayName = "Counter/Segmented Integer Counter (CTR/SIC)",
                    PaddingRequirement = PaddingRequirements.None,
                    AllowableBlockSizes = new[] { -1 },
                    IsAEADMode = false,
                    NonceReusePolicy = NonceReusePolicies.NotApplicable
                });
                SymmetricCipherModes.Add(BlockCipherModes.CTS_CBC.ToString(), new SymmetricCipherModeDescription {
                    Name = BlockCipherModes.CTS_CBC.ToString(),
                    DisplayName = "Ciphertext Stealing with Ciphertext Block Chaining (CTS-CBC)",
                    PaddingRequirement = PaddingRequirements.IfUnderOneBlock,
                    AllowableBlockSizes = new[] { -1 },
                    IsAEADMode = false,
                    NonceReusePolicy = NonceReusePolicies.NotApplicable
                });
                SymmetricCipherModes.Add(BlockCipherModes.OFB.ToString(), new SymmetricCipherModeDescription {
                    Name = BlockCipherModes.OFB.ToString(),
                    DisplayName = "Output Feedback (OFB)",
                    PaddingRequirement = PaddingRequirements.None,
                    AllowableBlockSizes = new[] { -1 },
                    IsAEADMode = false,
                    NonceReusePolicy = NonceReusePolicies.NotApplicable
                });
                // Add AEAD modes of operation
                SymmetricCipherModes.Add(AEADBlockCipherModes.EAX.ToString(), new SymmetricCipherModeDescription {
                    Name = AEADBlockCipherModes.EAX.ToString(),
                    DisplayName = "Counter with OMAC (EAX)",
                    PaddingRequirement = PaddingRequirements.None,
                    AllowableBlockSizes = new[] { 64, 128, 192, 256 },
                    IsAEADMode = true,
                    NonceReusePolicy = NonceReusePolicies.NotAllowed
                });
                SymmetricCipherModes.Add(AEADBlockCipherModes.GCM.ToString(), new SymmetricCipherModeDescription {
                    Name = AEADBlockCipherModes.GCM.ToString(),
                    DisplayName = "Galois/Counter Mode (GCM)",
                    PaddingRequirement = PaddingRequirements.None,
                    AllowableBlockSizes = new[] { 128 },
                    IsAEADMode = true,
                    NonceReusePolicy = NonceReusePolicies.NotAllowed
                });
                // TODO: Implement OCB mode
                /*
                Cryptography.SymmetricCipherModes.Add(AEADBlockCipherModes.OCB.ToString(), new SymmetricCipherModeDescription {
                    Name = AEADBlockCipherModes.OCB.ToString(),
                    DisplayName = "Offset CodeBook (OCB)",
                    PaddingRequirement = PaddingRequirements.None,
                    AllowableBlockSizes = new[] { 128, 192, 256 },
                    IsAEADMode = true,
                    NonceReusePolicy = NonceReusePolicies.NotAllowed
                });
                */
				// TODO: Implement SIV mode
				/*
                Cryptography.SymmetricCipherModes.Add(AEADBlockCipherModes.SIV.ToString(), new SymmetricCipherModeDescription {
                    Name = AEADBlockCipherModes.SIV.ToString(),
                    DisplayName = "Synthetic Initialisation Vector (SIV)",
                    PaddingRequirement = PaddingRequirements.None,
                    AllowableBlockSizes = new[] { 128, 192, 256 },
                    IsAEADMode = true,
                    NonceReusePolicy = NonceReusePolicies.Allowed
                });
                */

                // Add block cipher padding schemes
                SymmetricCipherPaddings.Add(BlockCipherPaddingTypes.ISO10126D2.ToString(), new SymmetricCipherPaddingDescription {
                    Name = BlockCipherPaddingTypes.ISO10126D2.ToString(),
                    DisplayName = "ISO 10126-2"
                });
                SymmetricCipherPaddings.Add(BlockCipherPaddingTypes.ISO7816D4.ToString(), new SymmetricCipherPaddingDescription {
                    Name = BlockCipherPaddingTypes.ISO7816D4.ToString(),
                    DisplayName = "ISO/IEC 7816-4"
                });
                SymmetricCipherPaddings.Add(BlockCipherPaddingTypes.PKCS7.ToString(), new SymmetricCipherPaddingDescription {
                    Name = BlockCipherPaddingTypes.PKCS7.ToString(),
                    DisplayName = "PKCS 7"
                });
                SymmetricCipherPaddings.Add(BlockCipherPaddingTypes.TBC.ToString(), new SymmetricCipherPaddingDescription {
                    Name = BlockCipherPaddingTypes.TBC.ToString(),
                    DisplayName = "Trailing Bit Complement (TBC)"
                });
                SymmetricCipherPaddings.Add(BlockCipherPaddingTypes.X923.ToString(), new SymmetricCipherPaddingDescription {
                    Name = BlockCipherPaddingTypes.X923.ToString(),
                    DisplayName = "ANSI X.923"
                });

                // Add key derivation schemes
                KeyDerivationFunctions.Add(ObscurCore.Cryptography.KeyDerivationFunctions.PBKDF2.ToString(), new KDFDescription {
                    Name = ObscurCore.Cryptography.KeyDerivationFunctions.PBKDF2.ToString(),
                    DisplayName = "Password Based Key Derivation Function 2 (PBKDF2)"
                });
                KeyDerivationFunctions.Add(ObscurCore.Cryptography.KeyDerivationFunctions.Scrypt.ToString(), new KDFDescription {
                    Name = ObscurCore.Cryptography.KeyDerivationFunctions.Scrypt.ToString(),
                    DisplayName = "Scrypt KDF by C. Percival"
                });

                // Add PRNGs
                PseudoRandomNumberGenerators.Add(ObscurCore.Cryptography.PseudoRandomNumberGenerators.Salsa20.ToString(), new PRNGDescription {
                    Name = ObscurCore.Cryptography.PseudoRandomNumberGenerators.Salsa20.ToString(),
                    DisplayName = "Salsa20 cipher-based PRNG"
                });
				PseudoRandomNumberGenerators.Add(ObscurCore.Cryptography.PseudoRandomNumberGenerators.SOSEMANUK.ToString(), new PRNGDescription {
					Name = ObscurCore.Cryptography.PseudoRandomNumberGenerators.SOSEMANUK.ToString(),
					DisplayName = "SOSEMANUK cipher-based PRNG"
				});
            }

            // Data storage.
            internal static Dictionary<string, SymmetricCipherDescription> SymmetricCiphers =
                new Dictionary<string, SymmetricCipherDescription>();
            internal static Dictionary<string, SymmetricCipherModeDescription> SymmetricCipherModes =
                new Dictionary<string, SymmetricCipherModeDescription>();
            internal static Dictionary<string, SymmetricCipherPaddingDescription> SymmetricCipherPaddings =
                new Dictionary<string, SymmetricCipherPaddingDescription>();
			internal static Dictionary<string, HashFunctionDescription> HashFunctions =
				new Dictionary<string, HashFunctionDescription>();
			internal static Dictionary<string, KDFDescription> KeyDerivationFunctions =
				new Dictionary<string, KDFDescription>();
			internal static Dictionary<string, PRNGDescription> PseudoRandomNumberGenerators =
				new Dictionary<string, PRNGDescription>();
			
            // Query functions
            public static bool IsSymmetricCipherSupported(string name) {
                return SymmetricCiphers.ContainsKey(name);
            }

            public static bool IsModeSupported(string name) {
				return SymmetricCipherModes.ContainsKey(name);
            }

            public static bool IsSymmetricPaddingSupported(string name) {
				return SymmetricCipherPaddings.ContainsKey(name);
            }
        }

		public static class Packaging
		{
			static Packaging () {
				// Add all symmetric block ciphers
				PayloadModules.Add (PayloadLayoutSchemes.Simple.ToString (), new PayloadModuleDescription {
					Name = PayloadLayoutSchemes.Simple.ToString(),
					DisplayName = PayloadLayoutSchemes.Simple.ToString()
				});
				PayloadModules.Add (PayloadLayoutSchemes.Frameshift.ToString (), new PayloadModuleDescription {
					Name = PayloadLayoutSchemes.Frameshift.ToString(),
					DisplayName = PayloadLayoutSchemes.Frameshift.ToString()
				});
				PayloadModules.Add (PayloadLayoutSchemes.Fabric.ToString (), new PayloadModuleDescription {
					Name = PayloadLayoutSchemes.Fabric.ToString(),
					DisplayName = PayloadLayoutSchemes.Fabric.ToString()
				});
			}
			
			// Data storage.
			public static Dictionary<string, PayloadModuleDescription> PayloadModules =
				new Dictionary<string, PayloadModuleDescription>();
			
			
		}
    }
}